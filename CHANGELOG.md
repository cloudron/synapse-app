[0.1.0]
* Initial version

[0.1.1]
* Removed the Riot part of the installation due to security
* Changed from Sqlite to Postgres
* Added LDAP support
* Fixed upload limit in nginx_matrix.conf
* Added bogus index.html so cloudron recognizes the matrix server as online
* Added coturn as TURN server

[0.1.2]
* Updated logo (transparent)
* Enabled email settings

[0.1.3]
* Changed log location of nginx due to backup crashes

[0.1.4]
* Changed log location of synapse due to backup crashes
* Updated some configs

[0.1.5]
* Update to synapse v0.31.2

[0.1.6]
* Update to synapse v0.33.8

[0.1.7]
* Update to synapse v0.33.9

[0.1.8]
* Update to synapse v0.99.0
* Fixed missing LDAP stuff

[0.1.9]
* Update to synapse v0.99.3

[0.2.0]
* Update to synapse v0.99.4

[0.2.1]
* Update to synapse v0.99.5.1

[0.2.2]
* Update to synapse v0.99.5.2

[0.2.3]
* Update to synapse v1.0.0
* Fixed url preview
* Fixed voip

[0.2.4]
* Fixed federation

[0.2.5]
* Update to synapse v1.1.0

[0.2.6]
* Update to synapse v1.2.1

[0.2.7]
* Update to synapse v1.3.0

[0.2.8]
* Update to synapse v1.3.1

[0.2.9]
* Update to synapse v1.4.0 (lots of changes)

[0.3.0]
* Update to synapse v1.4.1

[0.3.1]
* Update to synapse v1.5.1

[0.3.3]
* Update to synapse v1.6.0

[0.3.4]
* Update to synapse v1.6.1

[0.3.5]
* Update to synapse v1.8.0

[0.3.6]
* Update to synapse v1.9.1

[0.3.7]
* Update to synapse v1.10.0

[0.3.8]
* Update to synapse v1.11.0

[0.4.0]
* Update to synapse v1.12.0

[0.4.1]
* Update to synapse v1.12.2

[0.5.0]
* New reworked app

[0.6.0]
* Fix title

[0.7.0]
* Set turn_uris to an array and not a string

[0.7.1]
* Users will now automatically join the #discuss channel (only in new installations)

[1.0.0]
* Use latest base image
* Update to synapse v1.12.4

[1.1.0]
* Update Synapse to 1.13.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.13.0)
* Set Referrer-Policy header to no-referrer on media downloads. (#7009)
* Admin API POST /_synapse/admin/v1/join/<roomIdOrAlias> to join users to a room like auto_join_rooms for creation of users. (#7051)
* Add options to prevent users from changing their profile or associated 3PIDs. (#7096)
* Allow server admins to define and enforce a password policy (MSC2000). (#7118)
* Improve the support for SSO authentication on the login fallback page. (#7152, #7235)
* Always whitelist the login fallback in the SSO configuration if public_baseurl is set. (#7153)
* Admin users are no longer required to be in a room to create an alias for it. (#7191)
* Require admin privileges to enable room encryption by default. This does not affect existing rooms. (#7230)

[1.2.0]
* Update Synapse to 1.14.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.14.0)

[1.3.0]
* Add optional sso support

[1.4.0]
* Update Synapse to 1.15.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.15.0)
* Advertise support for Client-Server API r0.6.0 and remove related unstable feature flags. (#6585)
* Add an option to disable autojoining rooms for guest accounts. (#6637)
* Add admin APIs to allow server admins to manage users' devices. Contributed by @dklimpel. (#7481)
* Add support for generating thumbnails for WebP images. Previously, users would see an empty box instead of preview image. Contributed by @WGH-. (#7586)
* Support the standardized m.login.sso user-interactive authentication flow. (#7630)

[1.5.0]
* Update Synapse to 1.15.2
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.15.2)
* A malicious homeserver could force Synapse to reset the state in a room to a small subset of the correct state. This affects all Synapse deployments which federate with untrusted servers. (96e9afe6)
* HTML pages served via Synapse were vulnerable to clickjacking attacks. This predominantly affects homeservers with single-sign-on enabled, but all server administrators are encouraged to upgrade. (ea26e9a9)

[1.6.0]
* Update Synapse to 1.16.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.16.0)
* Add an option to enable encryption by default for new rooms. (#7639)
* Add support for running multiple media repository workers. See docs/workers.md for instructions. (#7706)
* Media can now be marked as safe from quarantined. (#7718)
* Expand the configuration options for auto-join rooms. (#7763)

[1.6.1]
* Update Synapse to 1.16.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.16.1)
* Drop table local_rejections_stream which was incorrectly added in Synapse 1.16.0. (#7816, b1beb3ff5)

[1.7.0]
* Update Synapse to 1.17.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.17.0)
* Fix inconsistent handling of upper and lower case in email addresses when used as identifiers for login, etc. Contributed by @dklimpel. (#7021)
* Fix "Tried to close a non-active scope!" error messages when opentracing is enabled. (#7732)
* Fix incorrect error message when database CTYPE was set incorrectly. (#7760)
* Fix to not ignore set_tweak actions in Push Rules that have no value, as permitted by the specification. (#7766)
* Fix synctl to handle empty config files correctly. Contributed by @kotovalexarian. (#7779)
* Fixes a long standing bug in worker mode where worker information was saved in the devices table instead of the original IP address and user agent. (#7797)
* Fix 'stuck invites' which happen when we are unable to reject a room invite received over federation. (#7804, #7809, #7810)

[1.8.0]
* Update Synapse to 1.18.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.18.0)
* Include room states on invite events that are sent to application services. Contributed by @Sorunome. (#6455)
* Add delete room admin endpoint (POST /_synapse/admin/v1/rooms/<room_id>/delete). Contributed by @dklimpel. (#7613, #7953)
* Add experimental support for running multiple federation sender processes. (#7798)
* Add the option to validate the iss and aud claims for JWT logins. (#7827)
* Add support for handling registration requests across multiple client reader workers. (#7830)
* Add an admin API to list the users in a room. Contributed by Awesome Technologies Innovationslabor GmbH. (#7842)
* Allow email subjects to be customised through Synapse's configuration. (#7846)
* Add the ability to re-activate an account from the admin API. (#7847, #7908)
* Support oEmbed for media previews. (#7920)

[1.9.0]
* Update Synapse to 1.19.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.19.0)
* Add option to allow server admins to join rooms which fail complexity checks. Contributed by @lugino-emeritus. (#7902)
* Add an option to purge room or not with delete room admin endpoint (POST /_synapse/admin/v1/rooms/<room_id>/delete). Contributed by @dklimpel. (#7964)
* Add rate limiting to users joining rooms. (#8008)
* Add a /health endpoint to every configured HTTP listener that can be used as a health check endpoint by load balancers. (#8048)
* Allow login to be blocked based on the values of SAML attributes. (#8052)
* Allow guest access to the GET /_matrix/client/r0/rooms/{room_id}/members endpoint, according to MSC2689. Contributed by Awesome Technologies Innovationslabor GmbH. (#7314)

[1.9.1]
* Update Synapse to 1.19.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.19.1)
* Fix a bug introduced in v1.19.0 where appservices with ratelimiting disabled would still be ratelimited when joining rooms. (#8139)
* Fix a bug introduced in v1.19.0 that would cause e.g. profile updates to fail due to incorrect application of rate limits on join requests. (#8153)

[1.10.0]
* Update Synapse to 1.19.3
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.19.3)
* Partially mitigate bug where newly joined servers couldn't get past events in a room when there is a malformed event. (#8350)
* Make index.html customizable

[1.11.0]
* Update Synapse to 1.20.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.20.0)
* Add an endpoint to query your shared rooms with another user as an implementation of MSC2666. (#7785)
* Iteratively encode JSON to avoid blocking the reactor. (#8013, #8116)
* Add support for shadow-banning users (ignoring any message send requests). (#8034, #8092, #8095, #8142, #8152, #8157, #8158, #8176)
* Use the default template file when its equivalent is not found in a custom template directory. (#8037, #8107, #8252)
* Add unread messages count to sync responses, as specified in MSC2654. (#8059, #8254, #8270, #8274)
* Optimise /federation/v1/user/devices/ API by only returning devices with encryption keys. (#8198)

[1.12.0]
* Update Synapse to 1.21.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.21.0)
* Require the user to confirm that their password should be reset after clicking the email confirmation link. (#8004)
* Add an admin API GET /_synapse/admin/v1/event_reports to read entries of table event_reports. Contributed by @dklimpel. (#8217)
* Consolidate the SSO error template across all configuration. (#8248, #8405)
* Add a configuration option to specify a whitelist of domains that a user can be redirected to after validating their email or phone number. (#8275, #8417)
* Add experimental support for sharding event persister. (#8294, #8387, #8396, #8419)
* Add the room topic and avatar to the room details admin API. (#8305)
* Add an admin API for querying rooms where a user is a member. Contributed by @dklimpel. (#8306)

[1.12.1]
* Updat Synapse to 1.21.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.21.1)

[1.12.2]
* Update Synapse to 1.21.2
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.21.2)
* Security: HTML pages served via Synapse were vulnerable to cross-site scripting (XSS) attacks. All server administrators are encouraged to upgrade
* Fix rare bug where sending an event would fail due to a racey assertion. (#8530)

[1.13.0]
* Update Synapse to 1.22.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.22.0)
* Add ability for ThirdPartyEventRules modules to query and manipulate whether a room is in the public rooms directory. (#8292, #8467)
* Add support for olm fallback keys (MSC2732). (#8312, #8501)
* Add support for running background tasks in a separate worker process. (#8369, #8458, #8489, #8513, #8544, #8599)
* Add support for device dehydration (MSC2697). (#8380)
* Add support for MSC2409, which allows sending typing, read receipts, and presence events to appservices. (#8437, #8590)
* Change default room version to "6", per MSC2788. (#8461)
* Add the ability to send non-membership events into a room via the ModuleApi. (#8479)
* Increase default upload size limit from 10M to 50M. Contributed by @Akkowicz. (#8502)
* Add support for modifying event content in ThirdPartyRules modules. (#8535, #8564)

[1.13.1]
* Update Synapse to 1.22.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.22.1)
* Fix a bug where an appservice may not be forwarded events for a room it was recently invited to. Broke in v1.22.0. (#8676)
* Fix Object of type frozendict is not JSON serializable exceptions when using third-party event rules. Broke in v1.22.0. (#8678)

[1.14.0]
* Update Synapse to 1.23.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.23.0)
* Add a push rule that highlights when a jitsi conference is created in a room. (#8286)
* Add an admin api to delete a single file or files that were not used for a defined time from server. Contributed by @dklimpel. (#8519)
* Split admin API for reported events (GET /_synapse/admin/v1/event_reports) into detail and list endpoints. This is a breaking change to #8217 which was introduced in Synapse v1.21.0. Those who already use this API should check their scripts. Contributed by @dklimpel. (#8539)
* Support generating structured logs via the standard logging configuration. (#8607, #8685)
* Add an admin API to allow server admins to list users' pushers. Contributed by @dklimpel. (#8610, #8689)
* Add an admin API GET /_synapse/admin/v1/users/<user_id>/media to get information about uploaded media. Contributed by @dklimpel. (#8647)
* Add an admin API for local user media statistics. Contributed by @dklimpel. (#8700)
* Add displayname to Shared-Secret Registration for admins. (#8722)

[1.14.1]
* Update Synapse to 1.23.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.23.1)
* There is a denial of service attack (CVE-2020-26257) against the federation APIs in which future events will not be correctly sent to other servers over federation. This affects all servers that participate in open federation. (Fixed in #8776).

[1.15.0]
* Update Synapse to 1.24.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.24.0)
* Add a maximum version for pysaml2 on Python 3.5

[1.16.0]
* Update Synapse to 1.25.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.25.0)
* Add an admin API that lets server admins get power in rooms in which local users have power. (#8756)
* Add optional HTTP authentication to replication endpoints. (#8853)
* Improve the error messages printed as a result of configuration problems for extension modules. (#8874)
* Add the number of local devices to Room Details Admin API. Contributed by @dklimpel. (#8886)
* Add X-Robots-Tag header to stop web crawlers from indexing media. Contributed by Aaron Raimist. (#8887)
* Spam-checkers may now define their methods as async. (#8890)
* Add support for allowing users to pick their own user ID during a single-sign-on login. (#8897, #8900, #8911, #8938, #8941, #8942, #8951)
* Add an email.invite_client_location configuration option to send a web client location to the invite endpoint on the identity server which allows customisation of the email template. (#8930)
* The search term in the list room and list user Admin APIs is now treated as case-insensitive. (#8931)
* Apply an IP range blacklist to push and key revocation requests. (#8821, #8870, #8954)
* Add an option to allow re-use of user-interactive authentication sessions for a period of time. (#8970)
* Allow running the redact endpoint on workers. (#8994)

[1.17.0]
* Update Synapse to 1.26.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.26.0)
* During user-interactive authentication via single-sign-on, give a better error if the user uses the wrong account on the SSO IdP. (#9091)
* Give the public_baseurl a default value, if it is not explicitly set in the configuration file. (#9159)
* Improve performance when calculating ignored users in large rooms. (#9024)
* Implement MSC2176 in an experimental room version. (#8984)
* Add an admin API for protecting local media from quarantine. (#9086)
* Remove a user's avatar URL and display name when deactivated with the Admin API. (#8932)

[1.18.0]
* Update Synapse to 1.27.0
* Use base image v3
* Update python to 3.8
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.27.0)
* Add an admin API for getting and deleting forward extremities for a room. (#9062)
* Add an admin API for retrieving the current room state of a room. (#9168)
* Add an admin API endpoint for shadow-banning users. (#9209)

[1.19.0]
* Update Synapse to 1.28.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.28.0)
* New admin API to get the context of an event: /_synapse/admin/rooms/{roomId}/context/{eventId}. (#9150)
* Further improvements to the user experience of registration via single sign-on. (#9300, #9301)
* Add hook to spam checker modules that allow checking file uploads and remote downloads. (#9311)
* Add support for receiving OpenID Connect authentication responses via form POSTs rather than GETs. (#9376)
* Add the shadow-banning status to the admin API for user info. (#9400)

[1.20.0]
* Update Synapse to 1.29.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.29.0)
* Add rate limiters to cross-user key sharing requests. (#8957)
* Add order_by to the admin API GET /_synapse/admin/v1/users/<user_id>/media. Contributed by @dklimpel. (#8978)
* Add some configuration settings to make users' profile data more private. (#9203)
* The no_proxy and NO_PROXY environment variables are now respected in proxied HTTP clients with the lowercase form taking precedence if both are present. Additionally, the lowercase https_proxy environment variable is now respected in proxied HTTP clients on top of existing support for the uppercase HTTPS_PROXY form and takes precedence if both are present. Contributed by Timothy Leung. (#9372)
* Add a configuration option, user_directory.prefer_local_users, which when enabled will make it more likely for users on the same server as you to appear above other users. (#9383, #9385)
* Add support for regenerating thumbnails if they have been deleted but the original image is still stored. (#9438)

[1.21.0]
* Update Synapse to 1.30.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.30.0)
* Add prometheus metrics for number of users successfully registering and logging in. (#9510, #9511, #9573)
* Add synapse_federation_last_sent_pdu_time and synapse_federation_last_received_pdu_time prometheus metrics, which monitor federation delays by reporting the timestamps of messages sent and received to a set of remote servers. (#9540)
* Add support for generating JSON Web Tokens dynamically for use as OIDC client secrets. (#9549)
* Optimise handling of incomplete room history for incoming federation. (#9601)
* Finalise support for allowing clients to pick an SSO Identity Provider (MSC2858). (#9617)
* Tell spam checker modules about the SSO IdP a user registered through if one was used. (#9626)

[1.21.1]
* Update Synapse to 1.30.1

[1.22.0]
* Update Synapse to 1.31.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.31.0)
* Add support to OpenID Connect login for requiring attributes on the userinfo response. Contributed by Hubbe King. (#9609)
* Add initial experimental support for a "space summary" API. (#9643, #9652, #9653)
* Add support for the busy presence state as described in MSC3026. (#9644)
* Add support for credentials for proxy authentication in the HTTPS_PROXY environment variable. (#9657)

[1.22.1]
* Update Synapse to 1.32.0
* Add a Synapse module for routing presence updates between users. (#9491)
* Add an admin API to manage ratelimit for a specific user. (#9648)
* Include request information in structured logging output. (#9654)
* Add order_by to the admin API GET /_synapse/admin/v2/users. Contributed by @dklimpel. (#9691)
* Replace the room_invite_state_types configuration setting with room_prejoin_state. (#9700)
* Add experimental support for MSC3083: restricting room access via group membership. (#9717, #9735)
* Update experimental support for Spaces: include m.room.create in the room state sent with room-invites. (#9710)
* Synapse now requires Python 3.6 or later. It also requires Postgres 9.6 or later or SQLite 3.22 or later. (#9766)
* Prevent synapse_forward_extremities and synapse_excess_extremity_events Prometheus metrics from initially reporting zero-values after startup. (#8926)
* Fix recently added ratelimits to correctly honour the application service rate_limited flag. (#9711)
* Fix longstanding bug which caused duplicate key value violates unique constraint "remote_media_cache_thumbnails_media_origin_media_id_thumbna_key" errors. (#9725)
* Fix bug where sharded federation senders could get stuck repeatedly querying the DB in a loop, using lots of CPU. (#9770)
* Fix duplicate logging of exceptions thrown during federation transaction processing. (#9780)

[1.22.2]
* Update Synapse to 1.32.0
* Add a Synapse module for routing presence updates between users. (#9491)
* Add an admin API to manage ratelimit for a specific user. (#9648)
* Include request information in structured logging output. (#9654)
* Add order_by to the admin API GET /_synapse/admin/v2/users. Contributed by @dklimpel. (#9691)
* Replace the room_invite_state_types configuration setting with room_prejoin_state. (#9700)
* Add experimental support for MSC3083: restricting room access via group membership. (#9717, #9735)
* Update experimental support for Spaces: include m.room.create in the room state sent with room-invites. (#9710)
* Synapse now requires Python 3.6 or later. It also requires Postgres 9.6 or later or SQLite 3.22 or later. (#9766)
* Prevent synapse_forward_extremities and synapse_excess_extremity_events Prometheus metrics from initially reporting zero-values after startup. (#8926)
* Fix recently added ratelimits to correctly honour the application service rate_limited flag. (#9711)
* Fix longstanding bug which caused duplicate key value violates unique constraint "remote_media_cache_thumbnails_media_origin_media_id_thumbna_key" errors. (#9725)
* Fix bug where sharded federation senders could get stuck repeatedly querying the DB in a loop, using lots of CPU. (#9770)
* Fix duplicate logging of exceptions thrown during federation transaction processing. (#9780)

[1.22.3]
* Update Synapse to 1.32.1
* Fix a regression in Synapse 1.32.0 which caused Synapse to report large numbers of Prometheus time series, potentially overwhelming Prometheus instances. (#9854)

[1.22.4]
* Update Synapse to 1.32.2
* Fix a regression in Synapse 1.32.0 and 1.32.1 which caused LoggingContext errors in plugins. (#9857)

[1.23.0]
* Update Synapse to 1.33.0
* Update experimental support for MSC3083: restricting room access via group membership. (#9800, #9814)
* Add experimental support for handling presence on a worker. (#9819, #9820, #9828, #9850)
* Return a new template when an user attempts to renew their account multiple times with the same token, stating that their account is set to expire. This replaces the invalid token template that would previously be shown in this case. This change concerns the optional account validity feature. (#9832)
* Fixes the OIDC SSO flow when using a public_baseurl value including a non-root URL path. (#9726)
* Fix thumbnail generation for some sites with non-standard content types. Contributed by @rkfg. (#9788)
* Add some sanity checks to identity server passed to 3PID bind/unbind endpoints. (#9802)
* Limit the size of HTTP responses read over federation. (#9833)
* Fix a bug which could cause Synapse to get stuck in a loop of resyncing device lists. (#9867)
* Fix a long-standing bug where errors from federation did not propagate to the client. (#9868)

[1.23.1]
* Update Synapse to 1.33.1
* Fix bug where /sync would break if using the latest version of attrs dependency, by pinning to a previous version. (#9937)

[1.23.2]
* Update Synapse to 1.33.2
* This release fixes a denial of service attack (CVE-2021-29471) against Synapse's push rules implementation.

[1.24.0]
* Update Synapse to 1.34.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.34.0)
* Add support for DELETE /_synapse/admin/v1/rooms/<room_id>. (#9889)
* Improve performance after joining a large room when presence is enabled. (#9910, #9916)
* Support stable identifiers for MSC1772 Spaces. m.space.child events will now be taken into account when populating the experimental spaces summary response. Please see the upgrade notes if you have customised room_invite_state_types in your configuration. (#9915, #9966)
* Improve performance of backfilling in large rooms. (#9935)

[1.25.0]
* Update Synapse to 1.35.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.35.0)
* Add experimental support to allow a user who could join a restricted room to view it in the spaces summary. (#9922, #10007, #10038)
* Reduce memory usage when joining very large rooms over federation. (#9958)
* Add a configuration option which allows enabling opentracing by user id. (#9978)
* Enable experimental support for MSC2946 (spaces summary API) and MSC3083 (restricted join rules) by default. (#10011)

[1.25.1]
* Update Synapse to 1.35.1
* Fix a bug introduced in v1.35.0 where invite-only rooms would be shown to all users in a space, regardless of if the user had access to it. (#10109)

[1.26.0]
* Update Synapse to 1.36.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.36.0)

[1.27.0]
* Update Synapse to 1.38.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.38.0)
* Implement refresh tokens as specified by MSC2918. (#9450)
* Add support for evicting cache entries based on last access time. (#10205)
* Omit empty fields from the /sync response. Contributed by @deepbluev7. (#10214)
* Improve validation on federation send_{join,leave,knock} endpoints. (#10225, #10243)
* Mark events received over federation which fail a spam check as "soft-failed". (#10263)
* Add metrics for new inbound federation staging area. (#10284)
* Add script to print information about recently registered users. (#10290)

[1.27.1]
* Update Synapse to 1.38.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.38.1)

[1.28.0]
* Update Synapse to 1.39.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.39.0)

[1.29.0]
* Update Synapse to 1.40.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.40.0)

[1.30.0]
* Update Synapse to 1.41.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.41.0)

[1.30.1]
* Send logs to the console
* Fix postinstall message about federation

[1.30.2]
* Update Synapse to 1.41.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.41.1)
* GHSA-3x4c-pq33-4w3q / CVE-2021-39164: Enumerating a private room's list of members and their display names.
* GHSA-jj53-8fmw-f2w2 / CVE-2021-39163: Disclosing a private room's name, avatar, topic, and number of members.

[1.31.0]
* Update Synapse to 1.42.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.42.0)
* Support room version 9 from MSC3375. (#10747)
* Add support for MSC3231: Token authenticated registration. Users can be required to submit a token during registration to authenticate themselves. Contributed by Callum Brown. (#10142)
* Add support for MSC3283: Expose enable_set_displayname in capabilities. (#10452)
* Port the PresenceRouter module interface to the new generic interface. (#10524)
* Add pagination to the spaces summary based on updates to MSC2946. (#10613, #10725)

[1.32.0]
* Update Synapse to 1.43.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.43.0)

[1.33.0]
* Update Synapse to 1.44.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.44.0)
* Only allow the MSC2716 /batch_send?chunk_id=xxx endpoint to connect to an already existing insertion event. (#10776)
* Improve oEmbed URL previews by processing the author name, photo, and video information. (#10814, #10819)
* Speed up responding with large JSON objects to requests. (#10868, #10905)
* Add a user_may_create_room_with_invites spam checker callback to allow modules to allow or deny a room creation request based on the invites and/or 3PID invites it includes. (#10898)

[1.34.0]
* Update Synapse to 1.45.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.45.0)
* Fix a long-standing bug when using multiple event persister workers where events were not correctly sent down /sync due to a race. (#11045)
* Fix a bug introduced in Synapse 1.45.0rc1 where the user directory would stop updating if it processed an event from a user not in the users table. (#11053)
* Fix a bug introduced in Synapse 1.44.0 when logging errors during oEmbed processing. (#11061)

[1.34.1]
* Update Synapse to 1.45.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.45.1)
* Revert change to counting of deactivated users towards the monthly active users limit, introduced in 1.45.0rc1. (#11127)

[1.35.0]
* Update Synapse to 1.46.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.46.0)
* Fix a bug introduced in v1.46.0rc1 where URL previews of some XML documents would fail. (#11196)

[1.36.0]
* Update Synapse to 1.47.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.47.0)
* Fix a bug introduced in 1.47.0rc1 which caused worker processes to not halt startup in the presence of outstanding database migrations. (#11346)
* Fix a bug introduced in 1.47.0rc1 which prevented the 'remove deleted devices from device_inbox column' background process from running when updating from a recent Synapse version. (#11303, #11353)

[1.36.1]
* Update Synapse to 1.47.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.47.1)
* GHSA-3hfw-x7gx-437c / CVE-2021-41281: Path traversal when downloading remote media.

[1.37.0]
* Update Synapse to 1.48.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.48.0)

[1.38.0]
* Update Synapse to 1.49.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.49.0)

[1.38.1]
* Update Synapse to 1.49.2
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.49.2)

[1.38.2]
* Update Synapse to 1.50.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.50.1)

[1.38.3]
* Update Synapse to 1.50.2
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.50.2)
* Fix a bug introduced in Synapse 1.40.0 that caused Synapse to fail to process incoming federation traffic after handling a large amount of events in a v1 room. (#11806)

[1.38.4]
* Update Synapse to 1.51.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.51.0)

[1.38.5]
* Update Synapse to 1.52.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.52.0)
* Remove account data (including client config, push rules and ignored users) upon user deactivation. (#11621, #11788, #11789)
* Add an admin API to reset connection timeouts for remote server. (#11639)
* Add an admin API to get a list of rooms that federate with a given remote homeserver. (#11658)
* Add a config flag to inhibit M_USER_IN_USE during registration. (#11743)
* Add a module callback to set username at registration. (#11790)
* Allow configuring a maximum file size as well as a list of allowed content types for avatars. (#11846)

[1.38.6]
* Update Synapse to 1.53.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.53.0)
* Add experimental support for sending to-device messages to application services, as specified by MSC2409. (#11215, #11966)
* Add a background database update to purge account data for deactivated users. (#11655)
* Experimental support for MSC3666: including bundled aggregations in server side search results. (#11837)
* Enable cache time-based expiry by default. The expiry_time config flag has been superseded by expire_caches and cache_entry_ttl. (#11849)
* Add a callback to allow modules to allow or forbid a 3PID (email address, phone number) from being associated to a local account. (#11854)
* Stabilize support and remove unstable endpoints for MSC3231. Clients must switch to the stable identifier and endpoint. See the upgrade notes for more information. (#11867)
* Allow modules to retrieve the current instance's server name and worker name. (#11868)
* Use a dedicated configurable rate limiter for 3PID invites. (#11892)
* Support the stable API endpoint for MSC3283: new settings in /capabilities endpoint. (#11933, #11989)
* Support the dir parameter on the /relations endpoint, per MSC3715. (#11941)
* Experimental implementation of MSC3706: extensions to /send_join to support reduced response size. (#11967)

[1.39.0]
* Update Synapse to 1.54.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.54.0)
* Fix a bug introduced in Synapse 1.54.0rc1 preventing the new module callbacks introduced in this release from being registered by modules. (#12141)
* Fix a bug introduced in Synapse 1.54.0rc1 where runtime dependency version checks would mistakenly check development dependencies if they were present and would not accept pre-release versions of dependencies. (#12129, #12177)

[1.40.0]
* Update Synapse to 1.55.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.55.0)
* Add third-party rules callbacks check_can_shutdown_room and check_can_deactivate_user. (#12028)
* Improve performance of logging in for large accounts. (#12132)
* Support the stable identifiers from MSC3440: threads. (#12151)
* Add a new Jinja2 template filter to extract the local part of an email address. (#12212)

[1.40.1]
* Update Synapse to 1.55.2
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.55.2)

[1.41.0]
* Update Synapse to 1.57.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.57.0)

[1.41.1]
* Update Synapse to 1.57.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.57.1)

[1.42.0]
* Update Synapse to 1.58.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.58.0)
* Implement MSC3383 for including the destination in server-to-server authentication headers. Contributed by @Bubu and @jcgruenhage for Famedly. (#11398)
* Enable processing of device list updates asynchronously. (#12365, #12465)
* Implement MSC2815 to allow room moderators to view redacted event content. Contributed by @tulir @ Beeper. (#12427)

[1.43.0]
* Update Synapse to 1.59.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.59.0)

[1.43.1]
* Update Synapse to 1.59.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.59.1)

[1.44.0]
* Update Synapse to 1.60.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.60.0)
* Add an option allowing users to use their password to reauthenticate for privileged actions even though password login is disabled. (#12883)
* Explicitly close ijson coroutines once we are done with them, instead of leaving the garbage collector to close them. (#12875)
* Improve URL previews by not including the content of media tags in the generated description. (#12887)

[1.45.0]
* Update Synapse to 1.61.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.61.0)
* Add new media_retention options to the homeserver config for routinely cleaning up non-recently accessed media. (#12732, #12972, #12977)
* Experimental support for MSC3772: Push rule for mutually related events. (#12740, #12859)
* Update to the check_event_for_spam module callback: Deprecate the current callback signature, replace it with a new signature that is both less ambiguous (replacing booleans with explicit allow/block) and more powerful (ability to return explicit error codes). (#12808)
* Add storage and module API methods to get monthly active users (and their corresponding appservices) within an optionally specified time range. (#12838, #12917)
* Support the new error code ORG.MATRIX.MSC3823.USER_ACCOUNT_SUSPENDED from MSC3823. (#12845, #12923)
* Add a configurable background job to delete stale devices. (#12855)
* Improve URL previews for pages with empty elements. (#12951)
* Allow updating a user's password using the admin API without logging out their devices. Contributed by @jcgruenhage. (#12952)

[1.45.1]
* Add s3 storage provider module

[1.45.2]
* Update Synapse to 1.61.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.61.1)
* Linkify GHSA commit

[1.46.0]
* Update Synapse to 1.62.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.62.0)

[1.47.0]
* Update Synapse to 1.63.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.63.0)

[1.47.1]
* Update Synapse to 1.63.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.63.1)
* Fix a bug introduced in Synapse 1.63.0 where push actions were incorrectly calculated for appservice users. This caused performance issues on servers with large numbers of appservices. (#13332)

[1.48.0]
* Update Synapse to 1.64.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.64.0)

[1.49.0]
* Update Synapse to 1.65.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.65.0)
* Add support for stable prefixes for MSC2285 (private read receipts). (#13273)
* Add new unstable error codes ORG.MATRIX.MSC3848.ALREADY_JOINED, ORG.MATRIX.MSC3848.NOT_JOINED, and ORG.MATRIX.MSC3848.INSUFFICIENT_POWER described in MSC3848. (#13343)
* Use stable prefixes for MSC3827. (#13370)
* Add a new module API method to translate a room alias into a room ID. (#13428)
* Add a new module API method to create a room. (#13429)

[1.49.1]
* Add oidc module

[1.49.2]
* Update Synapse to 1.66.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.66.0)

[1.50.0]
* Update Synapse to 1.67.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.67.0)
* Support setting the registration shared secret in a file, via a new `registration_shared_secret_path` configuration option.
* Change the default startup behaviour so that any missing "additional" configuration files (signing key, etc) are generated automatically.
* Improve performance of sending messages in rooms with thousands of local users.

[1.51.0]
* Update Synapse to 1.68.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.68.0)
* Fix packaging to include Cargo.lock in sdist. (#13909)
* Fix building from packaged sdist. Broken in v1.68.0rc1. (#13866)
* Fix the release script not publishing binary wheels. (#13850)
* Lower minimum supported rustc version to 1.58.1. (#13857)
* Lock Rust dependencies' versions. (#13858)
* Keep track of when we fail to process a pulled event over federation so we can intelligently back off in the future. (#13589, #13814)
* Add an admin API endpoint to fetch messages within a particular window of time. (#13672)
* Add an admin API endpoint to find a user based on their external ID in an auth provider. (#13810)
* Cancel the processing of key query requests when they time out. (#13680)
* Improve validation of request bodies for the following client-server API endpoints: /account/3pid/msisdn/requestToken, /org.matrix.msc3720/account_status, /account/3pid/add, /account/3pid/bind, /account/3pid/delete and /account/3pid/unbind. (#13687, #13736)
* Document the timestamp when a user accepts the consent, if consent tracking is used. (#13741)
* Add a listeners[x].request_id_header configuration option to specify which request header to extract and use as the request ID in order to correlate requests from a reverse proxy. (#13801)
* Fix a bug introduced in Synapse 1.41.0 where the /hierarchy API returned non-standard information (a room_id field under each entry in children_state). (#13506)
* Fix a long-standing bug where previously rejected events could end up in room state because they pass auth checks given the current state of the room. (#13723)
* Fix a long-standing bug where Synapse fails to start if a signing key file contains an empty line. (#13738)
* Fix a long-standing bug where Synapse would fail to handle malformed user IDs or room aliases gracefully in certain cases. (#13746)
* Fix a long-standing bug where device lists would remain cached when remote users left and rejoined the last room shared with the local homeserver. (#13749, #13826)
* Fix a long-standing bug that could cause stale caches in some rare cases on the first startup of Synapse with replication. (#13766)
* Fix a long-standing spec compliance bug where Synapse would accept a trailing slash on the end of /get_missing_events federation requests. (#13789)
* Delete associated data from event_failed_pull_attempts, insertion_events, insertion_event_extremities, insertion_event_extremities, insertion_event_extremities when purging the room. (#13825)

[1.52.0]
* Update Synapse to 1.69.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.69.0)
* Fix poor performance of the event_push_backfill_thread_id background update, which was introduced in Synapse 1.68.0rc1. (#14172, #14181)
* Fix an issue with Docker images causing the Rust dependencies to not be pinned correctly. Introduced in v1.68.0 (#14129)
* Fix a bug introduced in Synapse 1.69.0rc1 which would cause registration replication requests to fail if the worker sending the request is not running Synapse 1.69. (#14135)
* Fix error in background update when rotating existing notifications. Introduced in v1.69.0rc2. (#14138)
* Allow application services to set the origin_server_ts of a state event by providing the query parameter ts in PUT `/_matrix/client/r0/rooms/{roomId}/state/{eventType}/{stateKey}`, per MSC3316. Contributed by @lukasdenk. (#11866)
* Allow server admins to require a manual approval process before new accounts can be used (using MSC3866). (#13556)
* Exponentially backoff from backfilling the same event over and over. (#13635, #13936)
* Add cache invalidation across workers to module API. (#13667, #13947)
* Experimental implementation of MSC3882 to allow an existing device/session to generate a login token for use on a new device/session. (#13722, #13868)
* Experimental support for thread-specific receipts (MSC3771). (#13782, #13893, #13932, #13937, #13939)
* Add experimental support for MSC3881: Remotely toggle push notifications for another client. (#13799, #13831, #13860)
* Keep track when an event pulled over federation fails its signature check so we can intelligently back-off in the future. (#13815)
* Improve validation for the unspecced, internal-only `_matrix/client/unstable/add_threepid/msisdn/submit_token` endpoint. (#13832)
* Faster remote room joins: record when we first partial-join to a room. (#13892)
* Support a dir parameter on the /relations endpoint per MSC3715. (#13920)
* Ask mail servers receiving emails from Synapse to not send automatic replies (e.g. out-of-office responses). (#13957)
* Send push notifications for invites received over federation. (#13719, #14014)
* Fix a long-standing bug where typing events would be accepted from remote servers not present in a room. Also fix a bug where incoming typing events would cause other incoming events to get stuck during a fast join. (#13830)
* Fix a bug introduced in Synapse v1.53.0 where the experimental implementation of MSC3715 would give incorrect results when paginating forward. (#13840)
* Fix access token leak to logs from proxy agent. (#13855)
* Fix have_seen_event cache not being invalidated after we persist an event which causes inefficiency effects like extra /state federation calls. (#13863)
* Faster room joins: Fix a bug introduced in 1.66.0 where an error would be logged when syncing after joining a room. (#13872)
* Fix a bug introduced in 1.66.0 where some required fields in the pushrules sent to clients were not present anymore. Contributed by Nico. (#13904)
* Fix packaging to include Cargo.lock in sdist. (#13909)
* Fix a long-standing bug where device updates could cause delays sending out to-device messages over federation. (#13922)
* Fix a bug introduced in v1.68.0 where Synapse would require setuptools_rust at runtime, even though the package is only required at build time. (#13952)
* Fix a long-standing bug where POST `/_matrix/client/v3/keys/query` requests could result in excessively large SQL queries. (#13956)
* Fix a performance regression in the get_users_in_room database query. Introduced in v1.67.0. (#13972)
* Fix a bug introduced in v1.68.0 bug where Rust extension wasn't built in release mode when using poetry install. (#14009)
* Do not return an unspecified original_event field when using the stable /relations endpoint. Introduced in Synapse v1.57.0. (#14025)
* Correctly handle a race with device lists when a remote user leaves during a partial join. (#13885)
* Correctly handle sending local device list updates to remote servers during a partial join. (#13934)

[1.53.0]
* Update Synapse to 1.70.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.70.0)
* Support for [MSC3856](https://github.com/matrix-org/matrix-spec-proposals/pull/3856): threads list API. ([\#13394](https://github.com/matrix-org/synapse/issues/13394), [\#14171](https://github.com/matrix-org/synapse/issues/14171), [\#14175](https://github.com/matrix-org/synapse/issues/14175))
* Support for thread-specific notifications & receipts ([MSC3771](https://github.com/matrix-org/matrix-spec-proposals/pull/3771) and [MSC3773](https://github.com/matrix-org/matrix-spec-proposals/pull/3773)). ([\#13776](https://github.com/matrix-org/synapse/issues/13776), [\#13824](https://github.com/matrix-org/synapse/issues/13824), [\#13877](https://github.com/matrix-org/synapse/issues/13877), [\#13878](https://github.com/matrix-org/synapse/issues/13878), [\#14050](https://github.com/matrix-org/synapse/issues/14050), [\#14140](https://github.com/matrix-org/synapse/issues/14140), [\#14159](https://github.com/matrix-org/synapse/issues/14159), [\#14163](https://github.com/matrix-org/synapse/issues/14163), [\#14174](https://github.com/matrix-org/synapse/issues/14174), [\#14222](https://github.com/matrix-org/synapse/issues/14222))
* Stop fetching missing `prev_events` after we already know their signature is invalid. ([\#13816](https://github.com/matrix-org/synapse/issues/13816))
* Send application service access tokens as a header (and query parameter). Implements [MSC2832](https://github.com/matrix-org/matrix-spec-proposals/pull/2832). ([\#13996](https://github.com/matrix-org/synapse/issues/13996))
* Ignore server ACL changes when generating pushes. Implements [MSC3786](https://github.com/matrix-org/matrix-spec-proposals/pull/3786). ([\#13997](https://github.com/matrix-org/synapse/issues/13997))
* Experimental support for redirecting to an implementation of a [MSC3886](https://github.com/matrix-org/matrix-spec-proposals/pull/3886) HTTP rendezvous service. ([\#14018](https://github.com/matrix-org/synapse/issues/14018))
* The `/relations` endpoint can now be used on workers. ([\#14028](https://github.com/matrix-org/synapse/issues/14028))
* Advertise support for Matrix 1.3 and 1.4 on `/_matrix/client/versions`. ([\#14032](https://github.com/matrix-org/synapse/issues/14032), [\#14184](https://github.com/matrix-org/synapse/issues/14184))
* Improve validation of request bodies for the [Device Management](https://spec.matrix.org/v1.4/client-server-api/#device-management) and [MSC2697 Device Dehyrdation](https://github.com/matrix-org/matrix-spec-proposals/pull/2697) client-server API endpoints. ([\#14054](https://github.com/matrix-org/synapse/issues/14054))
* Experimental support for [MSC3874](https://github.com/matrix-org/matrix-spec-proposals/pull/3874): Filtering threads from the `/messages` endpoint. ([\#14148](https://github.com/matrix-org/synapse/issues/14148))
* Improve the validation of the following PUT endpoints: [`/directory/room/{roomAlias}`](https://spec.matrix.org/v1.4/client-server-api/#put_matrixclientv3directoryroomroomalias), [`/directory/list/room/{roomId}`](https://spec.matrix.org/v1.4/client-server-api/#put_matrixclientv3directorylistroomroomid) and [`/directory/list/appservice/{networkId}/{roomId}`](https://spec.matrix.org/v1.4/application-service-api/#put_matrixclientv3directorylistappservicenetworkidroomid). ([\#14179](https://github.com/matrix-org/synapse/issues/14179))
* Build and publish binary wheels for `aarch64` platforms. ([\#14212](https://github.com/matrix-org/synapse/issues/14212))

[1.53.1]
* Update Synapse to 1.70.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.70.1)
* Fix a bug introduced in Synapse 1.70.0rc1 where the access tokens sent to application services as headers were malformed. Application services which were obtaining access tokens from query parameters were not affected. (#14301)
* Fix room creation being rate limited too aggressively since Synapse v1.69.0. (#14314)

[1.54.0]
* Update Synapse to 1.71.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.71.0)

[1.55.0]
* Update Synapse to 1.72.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.72.0)
* Add experimental support for MSC3912: Relation-based redactions. (#14260)
* Add an Admin API endpoint for user lookup based on third-party ID (3PID). Contributed by @ashfame. (#14405)
* Faster joins: include heroes' membership events in the partial join response, for rooms without a name or canonical alias. (#14442)

[1.56.0]
* Update Synapse to 1.73.0
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.73.0)

[1.57.0]
* Update Synapse to 1.74.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.74.0)
* Improve user search for international display names. ([\#14464](https://github.com/matrix-org/synapse/issues/14464))
* Stop using deprecated `keyIds` parameter when calling `/_matrix/key/v2/server`. ([\#14490](https://github.com/matrix-org/synapse/issues/14490), [\#14525](https://github.com/matrix-org/synapse/issues/14525))
* Add new `push.enabled` config option to allow opting out of push notification calculation. ([\#14551](https://github.com/matrix-org/synapse/issues/14551), [\#14619](https://github.com/matrix-org/synapse/issues/14619))
* Advertise support for Matrix 1.5 on `/_matrix/client/versions`. ([\#14576](https://github.com/matrix-org/synapse/issues/14576))
* Improve opentracing and logging for to-device message handling. ([\#14598](https://github.com/matrix-org/synapse/issues/14598))
* Allow selecting "prejoin" events by state keys in addition to event types. ([\#14642](https://github.com/matrix-org/synapse/issues/14642))
* Fix a long-standing bug where a device list update might not be sent to clients in certain circumstances. ([\#14435](https://github.com/matrix-org/synapse/issues/14435), [\#14592](https://github.com/matrix-org/synapse/issues/14592), [\#14604](https://github.com/matrix-org/synapse/issues/14604))
* Suppress a spurious warning when `POST /rooms/<room_id>/<membership>/`, `POST /join/<room_id_or_alias`, or the unspecced `PUT /join/<room_id_or_alias>/<txn_id>` receive an empty HTTP request body. ([\#14600](https://github.com/matrix-org/synapse/issues/14600))
* Return spec-compliant JSON errors when unknown endpoints are requested. ([\#14620](https://github.com/matrix-org/synapse/issues/14620), [\#14621](https://github.com/matrix-org/synapse/issues/14621))
* Update html templates to load images over HTTPS. Contributed by @ashfame. ([\#14625](https://github.com/matrix-org/synapse/issues/14625))
* Fix a long-standing bug where the user directory would return 1 more row than requested. ([\#14631](https://github.com/matrix-org/synapse/issues/14631))
* Reject invalid read receipt requests with empty room or event IDs. Contributed by Nick @ Beeper (@fizzadar). ([\#14632](https://github.com/matrix-org/synapse/issues/14632))
* Fix a bug introduced in Synapse 1.67.0 where not specifying a config file or a server URL would lead to the `register_new_matrix_user` script failing. ([\#14637](https://github.com/matrix-org/synapse/issues/14637))
* Fix a long-standing bug where the user directory and room/user stats might be out of sync. ([\#14639](https://github.com/matrix-org/synapse/issues/14639), [\#14643](https://github.com/matrix-org/synapse/issues/14643))
* Fix a bug introduced in Synapse 1.72.0 where the background updates to add non-thread unique indexes on receipts would fail if they were previously interrupted. ([\#14650](https://github.com/matrix-org/synapse/issues/14650))
* Improve validation of field size limits in events. ([\#14664](https://github.com/matrix-org/synapse/issues/14664))
* Fix bugs introduced in Synapse 1.55.0 and 1.69.0 where application services would not be notified of events in the correct rooms, due to stale caches. ([\#14670](https://github.com/matrix-org/synapse/issues/14670))

[1.58.0]
* Update Synapse to 1.75.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.75.0)
* Fix race where calling /members or /state with an at parameter could fail for newly created rooms, when using multiple workers. (#14817)
* Add a cached function to synapse.module_api that returns a decorator to cache return values of functions. (#14663)
* Add experimental support for MSC3391 (removing account data). (#14714)
* Support RFC7636 Proof Key for Code Exchange for OAuth single sign-on. (#14750)
* Support non-OpenID compliant userinfo claims for subject and picture. (#14753)
* Improve performance of /sync when filtering all rooms, message types, or senders. (#14786)
* Improve performance of the /hierarchy endpoint. (#14263)
* Fix the MAU Limits section of the Grafana dashboard relying on a specific job name for the workers of a Synapse deployment. (#14644)
* Fix a bug introduced in Synapse 1.70.0 which could cause spurious UNIQUE constraint failed errors in the rotate_notifs background job. (#14669)
* Ensure stream IDs are always updated after caches get invalidated with workers. Contributed by Nick @ Beeper (@Fizzadar). (#14723)
* Remove the unspecced device field from /pushrules responses. (#14727)
* Fix a bug introduced in Synapse 1.73.0 where the picture_claim configured under oidc_providers was unused (the default value of "picture" was used instead). (#14751)
* Unescape HTML entities in URL preview titles making use of oEmbed responses. (#14781)
* Disable sending confirmation email when 3pid is disabled. (#14725)

[1.59.0]
* Update Synapse to 1.76.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.76.0)
* Faster joins: Fix a bug introduced in Synapse 1.69 where device list EDUs could fail to be handled after a restart when a faster join sync is in progress. (#14914)
* Update the default room version to v10 (MSC 3904). Contributed by @FSG-Cat. (#14111)
* Add a set_displayname() method to the module API for setting a user's display name. (#14629)
* Add a dedicated listener configuration for health endpoint. (#14747)
* Implement support for MSC3890: Remotely silence local notifications. (#14775)
* Implement experimental support for MSC3930: Push rules for (MSC3381) Polls. (#14787)
* Per MSC3925, bundle the whole of the replacement with any edited events, and optionally inhibit server-side replacement. (#14811)
* Faster joins: always serve a partial join response to servers that request it with the stable query param. (#14839)
* Faster joins: allow non-lazy-loading ("eager") syncs to complete after a partial join by omitting partial state rooms until they become fully stated. (#14870)
* Faster joins: request partial joins by default. Admins can opt-out of this for the time being---see the upgrade notes. (#14905)

[1.60.0]
* Update Synapse to 1.77.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.77.0)
* Fix bug where retried replication requests would return a failure. Introduced in v1.76.0. ([\#15024](https://github.com/matrix-org/synapse/issues/15024))

[1.61.0]
* Update Synapse to 1.78.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.78.0)
* Implement the experimental `exact_event_match` push rule condition from MSC3758. (#14964)
* Add account data to the command line user data export tool. (#14969)
* Implement MSC3873 to disambiguate push rule keys with dots in them. (#15004)
* Allow Synapse to use a specific Redis logical database in worker-mode deployments. (#15034)
* Tag opentracing spans for federation requests with the name of the worker serving the request. (#15042)
* Implement the experimental `exact_event_property_contains` push rule condition from MSC3966. (#15045)

[1.62.0]
* Update Synapse to 1.79.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.79.0)
* Fix a bug introduced in Synapse 1.79.0rc1 where attempting to register a on_remove_user_third_party_identifier module API callback would be a no-op. (#15227)
* Fix a rare bug introduced in Synapse 1.73 where events could remain unsent to other homeservers after a faster-join to a room. (#15248)
* Add two new Third Party Rules module API callbacks: on_add_user_third_party_identifier and on_remove_user_third_party_identifier. (#15044)
* Experimental support for MSC3967 to not require UIA for setting up cross-signing on first use. (#15077)
* Add media information to the command line user data export tool. (#15107)
* Add an admin API to delete a specific event report. (#15116)
* Add support for knocking to workers. (#15133)
* Allow use of the /filter Client-Server APIs on workers. (#15134)
* Update support for MSC2677: remove support for server-side aggregation of reactions. (#15172)
* Stabilise support for MSC3758: event_property_is push condition. (#15185)

[1.62.1]
* Update post installation message

[1.63.0]
* Update Synapse to 1.80.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.80.0)
* Fix a bug in which the POST `/_matrix/client/v3/rooms/{roomId}/report/{eventId}` endpoint would return the wrong error if the user did not have permission to view the event. This aligns Synapse's implementation with MSC2249. (#15298, #15300)
* Fix a bug introduced in Synapse 1.75.0rc1 where the SQLite port_db script
* would fail to open the SQLite database. (#15301)
* Stabilise support for MSC3966: event_property_contains push condition. (#15187)
* Implement MSC2659: application service ping endpoint. Contributed by Tulir @ Beeper. (#15249)
* Allow loading /register/available endpoint on workers. (#15268)
* Improve performance of creating and authenticating events. (#15195)
* Add topic and name events to group of events that are batch persisted when creating a room. (#15229)
* Fix a long-standing bug in which the user directory would assume any remote membership state events represent a profile change. (#14755, #14756)
* Implement MSC3873 to fix a long-standing bug where properties with dots were handled ambiguously in push rules. (#15190)
* Faster joins: Fix a bug introduced in Synapse 1.66 where spurious "Failed to find memberships ..." errors would be logged. (#15232)
* Fix a long-standing error when sending message into deleted room. (#15235)

[1.64.0]
* Update Synapse to 1.81.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.81.0)
* Fix the set_device_id_for_pushers_txn background update crash. (#15391)
* Add the ability to enable/disable registrations when in the OIDC flow. (#14978)
* Add a primitive helper script for listing worker endpoints. (#15243)
* Experimental support for passing One Time Key and device key requests to application services (MSC3983 and MSC3984). (#15314, #15321)
* Allow loading /password_policy endpoint on workers. (#15331)
* Add experimental support for Unix sockets. Contributed by Jason Little. (#15353)
* Build Debian packages for Ubuntu 23.04 (Lunar Lobster). (#15381)
* Fix a long-standing bug where edits of non-m.room.message events would not be correctly bundled. (#15295)
* Fix a bug introduced in Synapse v1.55.0 which could delay remote homeservers being able to decrypt encrypted messages sent by local users. (#15297)
* Add a check to SQLite port_db script
* to ensure that the sqlite database passed to the script exists before trying to port from it. (#15306)
* Fix a bug introduced in Synapse 1.76.0 where responses from worker deployments could include an internal `_INT_STREAM_POS` key. (#15309)
* Fix a long-standing bug that Synpase only used the legacy appservice routes. (#15317)
* Fix a long-standing bug preventing users from rejoining rooms after being banned and unbanned over federation. Contributed by Nico. (#15323)
* Fix bug in worker mode where on a rolling restart of workers the "typing" worker would consume 100% CPU until it got restarted. (#15332)
* Fix a long-standing bug where some to_device messages could be dropped when using workers. (#15349)
* Fix a bug introduced in Synapse 1.70.0 where the background sync from a faster join could spin for hours when one of the events involved had been marked for backoff. (#15351)
* Fix missing app variable in mail subject for password resets. Contributed by Cyberes. (#15352)
* Fix a rare bug introduced in Synapse 1.66.0 where initial syncs would fail when the user had been kicked from a faster joined room that had not finished syncing. (#15383)

[1.65.0]
* Update Synapse to 1.82.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.81.0)
* Allow loading the `/directory/room/{roomAlias}` endpoint on workers. ([\#15333](https://github.com/matrix-org/synapse/issues/15333))
* Add some validation to `instance_map` configuration loading. ([\#15431](https://github.com/matrix-org/synapse/issues/15431))
* Allow loading the `/capabilities` endpoint on workers. ([\#15436](https://github.com/matrix-org/synapse/issues/15436))
* Delete server-side backup keys when deactivating an account. ([\#15181](https://github.com/matrix-org/synapse/issues/15181))
* Fix and document untold assumption that `on_logged_out` module hooks will be called before the deletion of pushers. ([\#15410](https://github.com/matrix-org/synapse/issues/15410))
* Improve robustness when handling a perspective key response by deduplicating received server keys. ([\#15423](https://github.com/matrix-org/synapse/issues/15423))
* Synapse now correctly fails to start if the config option `app_service_config_files` is not a list. ([\#15425](https://github.com/matrix-org/synapse/issues/15425))
* Disable loading `RefreshTokenServlet` (`/_matrix/client/(r0|v3|unstable)/refresh`) on workers. ([\#15428](https://github.com/matrix-org/synapse/issues/15428))

[1.66.0]
* Update Synapse to 1.83.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.83.0)
* Experimental support to recursively provide relations per [MSC3981](https://github.com/matrix-org/matrix-spec-proposals/pull/3981). ([\#15315](https://github.com/matrix-org/synapse/issues/15315))
* Experimental support for [MSC3970](https://github.com/matrix-org/matrix-spec-proposals/pull/3970): Scope transaction IDs to devices. ([\#15318](https://github.com/matrix-org/synapse/issues/15318))
* Add an [admin API endpoint](https://matrix-org.github.io/synapse/v1.83/admin_api/experimental_features.html) to support per-user feature flags. ([\#15344](https://github.com/matrix-org/synapse/issues/15344))
* Add a module API to send an HTTP push notification. ([\#15387](https://github.com/matrix-org/synapse/issues/15387))
* Add an [admin API endpoint](https://matrix-org.github.io/synapse/v1.83/admin_api/statistics.html#get-largest-rooms-by-size-in-database) to query the largest rooms by disk space used in the database. ([\#15482](https://github.com/matrix-org/synapse/issues/15482))
* Disable push rule evaluation for rooms excluded from sync. ([\#15361](https://github.com/matrix-org/synapse/issues/15361))
* Fix a long-standing bug where cached server key results which were directly fetched would not be properly re-used. ([\#15417](https://github.com/matrix-org/synapse/issues/15417))
* Fix a bug introduced in Synapse 1.73.0 where some experimental push rules were returned by default. ([\#15494](https://github.com/matrix-org/synapse/issues/15494))

[1.67.0]
* Update Synapse to 1.84.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.84.0)
* Fix a bug introduced in Synapse 1.84.0rc1 where errors during startup were not reported correctly on Python < 3.10. ([\#15599](https://github.com/matrix-org/synapse/issues/15599))
* Add an option to prevent media downloads from configured domains. ([\#15197](https://github.com/matrix-org/synapse/issues/15197))
* Add `forget_rooms_on_leave` config option to automatically forget rooms when users leave them or are removed from them. ([\#15224](https://github.com/matrix-org/synapse/issues/15224))
* Add redis TLS configuration options. ([\#15312](https://github.com/matrix-org/synapse/issues/15312))
* Add a config option to delay push notifications by a random amount, to discourage time-based profiling. ([\#15516](https://github.com/matrix-org/synapse/issues/15516))
* Stabilize support for [MSC2659](https://github.com/matrix-org/matrix-spec-proposals/pull/2659): application service ping endpoint. Contributed by Tulir @ Beeper. ([\#15528](https://github.com/matrix-org/synapse/issues/15528))
* Implement [MSC4009](https://github.com/matrix-org/matrix-spec-proposals/pull/4009) to expand the supported characters in Matrix IDs. ([\#15536](https://github.com/matrix-org/synapse/issues/15536))
* Advertise support for Matrix 1.6 on `/_matrix/client/versions`. ([\#15559](https://github.com/matrix-org/synapse/issues/15559))
* Print full error and stack-trace of any exception that occurs during startup/initialization. ([\#15569](https://github.com/matrix-org/synapse/issues/15569))
* Don't fail on federation over TOR where SRV queries are not supported. Contributed by Zdzichu. ([\#15523](https://github.com/matrix-org/synapse/issues/15523))
* Experimental support for [MSC4010](https://github.com/matrix-org/matrix-spec-proposals/pull/4010) which rejects setting the `"m.push_rules"` via account data. ([\#15554](https://github.com/matrix-org/synapse/issues/15554), [\#15555](https://github.com/matrix-org/synapse/issues/15555))
* Fix a long-standing bug where an invalid membership event could cause an internal server error. ([\#15564](https://github.com/matrix-org/synapse/issues/15564))
* Require at least poetry-core v1.1.0. ([\#15566](https://github.com/matrix-org/synapse/issues/15566), [\#15571](https://github.com/matrix-org/synapse/issues/15571))

[1.67.1]
* Update Synapse to 1.84.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.84.1)
* Fix a bug introduced in Synapse v1.84.0 where workers do not start up when no `instance_map` was provided

[1.68.0]
* Update Synapse to 1.85.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.85.0)
* GHSA-26c5-ppr8-f33p / CVE-2023-32682 — Low Severity
* GHSA-98px-6486-j7qc / CVE-2023-32683 — Low Severity
* Fix a performance issue introduced in Synapse v1.83.0 which meant that purging rooms was very slow and database-intensive. (#15693)
* Improve performance of backfill requests by performing backfill of previously failed requests in the background. (#15585)
* Add a new admin API to create a new device for a user. (#15611)

[1.68.1]
* Update Synapse to 1.85.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.85.1)
* Fix bug in schema delta that broke upgrades for some deployments. Introduced in v1.85.0. (#15738, #15739)

[1.68.2]
* Update Synapse to 1.85.2
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.85.2)
* Fix regression where using TLS for HTTP replication between workers did not work. Introduced in v1.85.0. (#15746)

[1.69.0]
* Update Synapse to 1.86.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.86.0)
* Fix an error when having workers of different versions running. (#15774)
* Stable support for MSC3882 to allow an existing device/session to generate a login token for use on a new device/session. (#15388)
* Support resolving a room's canonical alias via the module API. (#15450)
* Enable support for MSC3952: intentional mentions. (#15520)
* Experimental MSC3861 support: delegate auth to an OIDC provider. (#15582)
* Add Synapse version deploy annotations to Grafana dashboard which enables easy correlation between behavior changes witnessed in a graph to a certain Synapse version and nail down regressions. (#15674)
* Add a catch-all * to the supported relation types when redacting an event and its related events. This is an update to MSC3912 implementation. (#15705)
* Speed up /messages by backfilling in the background when there are no backward extremities where we are directly paginating. (#15710)
* Expose a metric reporting the database background update status. (#15740)
* Correctly clear caches when we delete a room. (#15609)
* Check permissions for enabling encryption earlier during room creation to avoid creating broken rooms. (#15695)

[1.70.0]
* Update Synapse to 1.87.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.87.0)
* Improve /messages response time by avoiding backfill when we already have messages to return. (#15737)
* Add spam checker module API for logins. (#15838)
* Fix a long-standing bug where media files were served in an unsafe manner. Contributed by @joshqou. (#15680)
* Avoid invalidating a cache that was just prefilled. (#15758)
* Fix requesting multiple keys at once over federation, related to MSC3983. (#15770)
* Fix joining rooms through aliases where the alias server isn't a real homeserver. Contributed by @tulir @ Beeper. (#15776)

[1.70.1]
* Add workaround for broken thumbnailing
* Update s3 storage provider

[1.71.0]
* Update Synapse to 1.88.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.88.0)
* Add not_user_type param to the list accounts admin API. (#15844)
* Pin pydantic to ^=1.7.4 to avoid backwards-incompatible API changes from the 2.0.0 release.
* Contributed by @PaarthShah. (#15862)
* Correctly resize thumbnails with pillow version >=10. (#15876)

[1.72.0]
* Update Synapse to 1.89.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.89.0)
* Add Unix Socket support for HTTP Replication Listeners. Document and provide usage instructions for utilizing Unix sockets in Synapse. Contributed by Jason Little. (#15708, #15924)
* Allow + in Matrix IDs, per MSC4009. (#15911)
* Support room version 11 from MSC3820. (#15912)
* Allow configuring the set of workers to proxy outbound federation traffic through via outbound_federation_restricted_to. (#15913, #15969)
* Implement MSC3814, dehydrated devices v2/shrivelled sessions and move MSC2697 behind a config flag. Contributed by Nico from Famedly, H-Shay and poljar. (#15929)
* Fix a long-standing bug where remote invites weren't correctly pushed. (#15820)
* Fix background schema updates failing over a large upgrade gap. (#15887)
* Fix a bug introduced in 1.86.0 where Synapse starting with an empty experimental_features configuration setting. (#15925)
* Fixed deploy annotations in the provided Grafana dashboard config, so that it shows for any homeserver and not just matrix.org. Contributed by @wrjlewis. (#15957)
* Ensure a long state res does not starve CPU by occasionally yielding to the reactor. (#15960)
* Properly handle redactions of creation events. (#15973)
* Fix a bug where resyncing stale device lists could block responding to federation transactions, and thus delay receiving new data from the remote server. (#15975)

[1.73.0]
* Update Synapse to 1.90.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.90.0)
* Scope transaction IDs to devices (implement MSC3970). (#15629)
* Remove old rows from the cache_invalidation_stream_by_instance table automatically (this table is unused in SQLite). (#15868)
* Fix a long-standing bug where purging history and paginating simultaneously could lead to database corruption when using workers. (#15791)
* Fix a long-standing bug where profile endpoint returned a 404 when the user's display name was empty. (#16012)
* Fix a long-standing bug where the synapse_port_db failed to configure sequences for application services and partial stated rooms. (#16043)
* Fix long-standing bug with deletion in dehydrated devices v2. (#16046)

[1.74.0]
* Turn addon can be optionally enabled/disabled

[1.75.0]
* Update Synapse to 1.91.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.91.0)

[1.75.1]
* Update Synapse to 1.91.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.91.1)
* Fix a performance regression introduced in Synapse 1.91.0 where event persistence would cause an excessive linear growth in CPU usage. (#16220)

[1.75.2]
* Update Synapse to 1.91.2
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.91.2)
* Revert MSC3861 introspection cache, admin impersonation and account lock. (#16258)

[1.76.0]
* Update Synapse to 1.92.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.92.0)
* Revert MSC3861 introspection cache, admin impersonation and account lock. (#16258)
* Fix incorrect docstring for Ratelimiter. (#16255)

[1.76.1]
* Update Synapse to 1.92.2
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.92.2)

[1.76.2]
* Update Synapse to 1.92.3
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.92.3)
* Pillow 10.0.1 is now mandatory because of libwebp CVE-2023-4863, since Pillow provides libwebp in the wheels. (#16347)

[1.77.0]
* Update Synapse to 1.93.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.93.0)
* GHSA-4f74-84v3-j9q5 / CVE-2023-41335 — Low Severity  Temporary storage of plaintext passwords during password changes.
* GHSA-7565-cq32-vx2x / CVE-2023-42453 — Low Severity Improper validation of receipts allows forged read receipts.
* Add automatic purge after all users have forgotten a room. (#15488)
* Restore room purge/shutdown after a Synapse restart. (#15488)
* Support resolving homeservers using matrix-fed DNS SRV records from MSC4040. (#16137)
* Add the ability to use G (GiB) and T (TiB) suffixes in configuration options that refer to numbers of bytes. (#16219)
* Add span information to requests sent to appservices. Contributed by MTRNord. (#16227)
* Add the ability to enable/disable registrations when using CAS. Contributed by Aurélien Grimpard. (#16262)
* Allow the /notifications endpoint to be routed to workers. (#16265)

[1.78.0]
* Update base image to 4.2.0

[1.79.0]
* Update Synapse to 1.94.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.94.0)
* Render plain, CSS, CSV, JSON and common image formats in the browser (inline) when requested through the /download endpoint. (#15988)
* Add experimental support for MSC4028 to push all encrypted events to clients. (#16361)
* Minor performance improvement when sending presence to federated servers. (#16385)
* Minor performance improvement by caching server ACL checking. (#16360)

[1.80.0]
* Update Synapse to 1.95.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.95.0)
* Remove legacy unspecced `knock_state_events` field returned in some responses. (#16403)
* Fix a bug introduced in Synapse 1.81.0 where an AttributeError would be raised when `_matrix/client/v3/account/whoami` is called over a unix socket. Contributed by @Sir-Photch. (#16404)
* Properly return inline media when content types have parameters. (#16440)
* Prevent the purging of large rooms from timing out when Postgres is in use. The timeout which causes this issue was introduced in Synapse 1.88.0. (#16455)
* Improve the performance of purging rooms, particularly encrypted rooms. (#16457)
* Fix a bug introduced in Synapse 1.59.0 where servers could be incorrectly marked as available after an error response was received. (#16506)

[1.80.1]
* Update Synapse to 1.95.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.95.1)
* GHSA-mp92-3jfm-3575 / CVE-2023-43796 — Moderate Severity

[1.81.0]
* Update Synapse to 1.96.1
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.96.1)
* Add experimental support to allow multiple workers to write to receipts stream. (#16432)
* Add a new module API for controller presence. (#16544)
* Add a new module API callback that allows adding extra fields to events' unsigned section when sent down to clients. (#16549)
* Improve the performance of claiming encryption keys. (#16565, #16570)

[1.82.0]
* Switch LDAP authentication to OIDC login

[1.83.0]
* Update Synapse to 1.97.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.97.0)
* Add support for asynchronous uploads as defined by MSC2246. Contributed by @sumnerevans at @beeper. (#15503)
* Improve the performance of some operations in multi-worker deployments. (#16613, #16616)
* Fix a long-standing bug where some queries updated the same row twice. Introduced in Synapse 1.57.0. (#16609)
* Fix a long-standing bug where Synapse would not unbind third-party identifiers for Application Service users when deactivated and would not emit a compliant response. (#16617)
* Fix sending out of order POSITION over replication, causing additional database load. (#16639)

[1.84.0]
* Update Synapse to 1.98.0
* [Full changelog](https://github.com/matrix-org/synapse/releases/tag/v1.98.0)
* Synapse now declares support for Matrix v1.7, v1.8, and v1.9. (#16707)
* Add `on_user_login` module API callback for when a user logs in. (#15207)
* Support MSC4069: Inhibit profile propagation. (#16636)
* Restore tracking of requests and monthly active users when delegating authentication via MSC3861 to an OIDC provider. (#16672)
* Add an autojoin setting for server notices rooms, so users may be joined directly instead of receiving an invite. (#16699)
* Follow redirects when downloading media over federation (per MSC3860). (#16701)

[1.85.0]
* Update public suffix list as part of the base image to get the latest domains

[1.86.0]
* Update Synapse to 1.99.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.99.0)
* Add config options to set the avatar and the topic of the server notices room, as well as the avatar of the server notices user. (\https://github.com/element-hq/synapse/issues/16679)
* Add config option email.notif_delay_before_mail to tweak the delay before an email is sent following a notification. (\https://github.com/element-hq/synapse/issues/16696)
* Add new configuration option sentry.environment for improved system monitoring. Contributed by @zeeshanrafiqrana. (\https://github.com/element-hq/synapse/issues/16738)
* Filter out rooms from the room directory being served to other homeservers when those rooms block that homeserver by their Access Control Lists. (\https://github.com/element-hq/synapse/pull/16759)
* Fix a long-standing bug where the signing keys generated by Synapse were world-readable. Contributed by Fabian Klemp. (\https://github.com/element-hq/synapse/issues/16740)
* Fix email verification redirection. Contributed by Fadhlan Ridhwanallah. (\https://github.com/element-hq/synapse/pull/16761)
* Fixed a bug that prevented users from being queried by display name if it contains non-ASCII characters. (\https://github.com/element-hq/synapse/pull/16767)
* Allow reactivate user without password with Admin API in some edge cases. (\https://github.com/element-hq/synapse/pull/16770)
* Adds the recursion_depth parameter to the response of the /relations endpoint if MSC3981 recursion is being performed. (\https://github.com/element-hq/synapse/pull/16775)
* Added version picker for Synapse documentation. Contributed by @Dmytro27Ind. (\https://github.com/element-hq/synapse/issues/16533)
* Clarify that password_config.enabled: "only_for_reauth" does not allow new logins to be created using password auth. (\https://github.com/element-hq/synapse/issues/16737)
* Remove value from header in configuration documentation for refresh_token_lifetime. (\https://github.com/element-hq/synapse/pull/16763)
* Add another custom statistics collection server to the documentation. Contributed by @loelkes. (\https://github.com/element-hq/synapse/pull/16769)
* Remove run-once workflow after adding the version picker to the documentation. (\https://github.com/element-hq/synapse/pull/9453)
* Update the implementation of [MSC2965](matrix-org/matrix-spec-proposals#2965) (OIDC Provider discovery). (\https://github.com/element-hq/synapse/issues/16726)
* Move the rust stubs inline for better IDE integration. (\https://github.com/element-hq/synapse/pull/16757)
* Fix sample config doc CI. (\https://github.com/element-hq/synapse/pull/16758)
* Simplify event internal metadata class. (\https://github.com/element-hq/synapse/pull/16762, \https://github.com/element-hq/synapse/pull/16780)
* Sign the published docker image using cosign. (\https://github.com/element-hq/synapse/pull/16774)
* Port EventInternalMetadata class to Rust. (\https://github.com/element-hq/synapse/pull/16782)
* Bump actions/setup-go from 4 to 5. (\https://github.com/element-hq/synapse/issues/16749)
* Bump actions/setup-python from 4 to 5. (\https://github.com/element-hq/synapse/issues/16748)
* Bump immutabledict from 3.0.0 to 4.0.0. (\https://github.com/element-hq/synapse/issues/16743)
* Bump isort from 5.12.0 to 5.13.0. (\https://github.com/element-hq/synapse/issues/16745)
* Bump isort from 5.13.0 to 5.13.1. (\https://github.com/element-hq/synapse/issues/16752)
* Bump pydantic from 2.5.1 to 2.5.2. (\https://github.com/element-hq/synapse/issues/16747)
* Bump ruff from 0.1.6 to 0.1.7. (\https://github.com/element-hq/synapse/issues/16746)
* Bump types-setuptools from 68.2.0.2 to 69.0.0.0. (\https://github.com/element-hq/synapse/issues/16744)

[1.87.0]
* Update Synapse to 1.100.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.100.0)
* Fix database performance regression due to changing Postgres table statistics. Introduced in v1.100.0rc1. (#16849)
* Advertise experimental support for MSC4028 through /matrix/clients/versions if enabled. Contributed by @hanadi92. (#16787)
* Handle wildcard type filters properly for room messages endpoint. Contributed by Mo Balaa. (#14984)

[1.88.0]
* Update Synapse to 1.101.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.101.0)
* Add support for stabilised MSC3981 that adds a recurse parameter on the /relations API. (#16842)
* Fix performance regression when fetching auth chains from the DB. Introduced in v1.100.0. (#16893)

[1.89.0]
* Update Synapse to 1.102.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.102.0)
* A metric was added for emails sent by Synapse, broken down by type: `synapse_emails_sent_total`. Contributed by Remi Rampin. (#16881)
* Do not send multiple concurrent requests for keys for the same server. (#16894)
* Fix performance issue when joining very large rooms that can cause the server to lock up. Introduced in v1.100.0. (#16903)
* Always prefer unthreaded receipt when >1 exist (MSC4102). (#16927)

[1.90.0]
* Update Synapse to 1.103.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.103.0)
* Add a new List Accounts v3 Admin API with improved deactivated user filtering capabilities. (#16874)
* Include Retry-After header by default per MSC4041. Contributed by @clokep. (#16947)
* Fix joining remote rooms when a module uses the `on_new_event` callback. This callback may now pass partial state events instead of the full state for remote rooms. Introduced in v1.76.0. (#16973)
* Fix performance issue when joining very large rooms that can cause the server to lock up. Introduced in v1.100.0. Contributed by @ggogel. (#16968)

[1.91.0]
* Update Synapse to 1.104.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.104.0)
* Fix regression when using OIDC provider. Introduced in v1.104.0rc1. (#17031)
* Add an OIDC config to specify extra parameters for the authorization grant URL. IT can be useful to pass an ACR value for example. (#16971)
* Add support for OIDC provider returning JWT. (#16972, #17031)
* Fix a bug which meant that, under certain circumstances, we might never retry sending events or to-device messages over federation after a failure. (#16925)
* Fix various long-standing bugs which could cause incorrect state to be returned from /sync in certain situations. (#16949)
* Fix case in which m.fully_read marker would not get updated. Contributed by @SpiritCroc. (#16990)
* Fix bug which did not retract a user's pending knocks at rooms when their account was deactivated. Contributed by @hanadi92. (#17010)

[1.91.1]
* Update Synapse to 1.105.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.105.0)
* Stabilize support for MSC4010 which clarifies the interaction of push rules and account data. Contributed by @clokep. (#17022)
* Stabilize support for MSC3981: /relations recursion. Contributed by @clokep. (#17023)
* Add support for moving /pushrules off of main process. (#17037, #17038)
* Fix various long-standing bugs which could cause incorrect state to be returned from /sync in certain situations. (#16930, #16932, #16942, #17064, #17065, #17066)
* Fix server notice rooms not always being created as unencrypted rooms, even when encryption_enabled_by_default_for_room_type is in use (server notices are always unencrypted). (#17033)
* Fix the .m.rule.encrypted_room_one_to_one and .m.rule.room_one_to_one default underride push rules being in the wrong order. Contributed by @Sumpy1. (#17043)

[1.91.2]
* Update Synapse to 1.105.1
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.105.1)
* GHSA-3h7q-rfh9-xm4v / CVE-2024-31208 — High Severity . Weakness in auth chain indexing allows DoS from remote room members through disk fill and high CPU usage.

[1.92.0]
* Update Synapse to 1.106.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.106.0)
* Send an email if the address is already bound to an user account. (#16819)
* Implement the rendezvous mechanism described by MSC4108. (#17056)
* Support delegating the rendezvous mechanism described MSC4108 to an external implementation. (#17086)
* Add validation to ensure that the limit parameter on /publicRooms is non-negative. (#16920)
* Return 400 M_NOT_JSON upon receiving invalid JSON in query parameters across various client and admin endpoints, rather than an internal server error. (#16923)
* Make the CSAPI endpoint /keys/device_signing/upload idempotent. (#16943)
* Redact membership events if the user requested erasure upon deactivating. (#17076)

[1.93.0]
* Update Synapse to 1.107.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.107.0)

[1.94.0]
* Update Synapse to 1.108.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.108.0)
* Add a feature that allows clients to query the configured federation whitelist. Disabled by default. (#16848, #17199)
* Add the ability to allow numeric user IDs with a specific prefix when in the CAS flow. Contributed by Aurélien Grimpard. (#17098)
* Fix bug where push rules would be empty in /sync for some accounts. Introduced in v1.93.0. (#17142)
* Add support for optional whitespace around the Federation API's Authorization header's parameter commas. (#17145)
* Fix bug where disabling room publication prevented public rooms being created on workers. (#17177, #17184)

[1.95.0]
* Update Synapse to 1.109.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.109.0)

[1.96.0]
* Update Synapse to 1.110.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.110.0)

[1.97.0]
* Update Synapse to 1.111.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.111.0)

[1.97.1]
* Update Synapse to 1.111.1
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.111.1)

[1.97.2]
* Update Synapse to 1.112.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.112.0)

[1.97.3]
* Update Synapse to 1.113.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.113.0)

[1.97.4]
* Update Synapse to 1.114.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.114.0)

[1.97.5]
* Update Synapse to 1.115.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.115.0)

[1.97.6]
* Update Synapse to 1.116.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.116.0)

[1.98.0]
* Update Synapse to 1.118.0
* [Full changelog](https://github.com/element-hq/synapse/releases/tag/v1.118.0)

[1.98.1]
* Update S3 Storage Provider to 1.5.0
[1.99.0]
* Update synapse to 1.119.0
* [Full Changelog](https://github.com/element-hq/synapse/releases/tag/v1.118.0)
* Support [MSC4151](https://github.com/matrix-org/matrix-spec-proposals/pull/4151)'s stable report room API. ([#&#8203;17374](https://github.com/element-hq/synapse/issues/17374))
* Add experimental support for [MSC4222](https://github.com/matrix-org/matrix-spec-proposals/pull/4222) (Adding `state_after` to sync v2). ([#&#8203;17888](https://github.com/element-hq/synapse/issues/17888))
* Fix bug with sliding sync where `$LAZY`-loading room members would not return `required_state` membership in incremental syncs. ([#&#8203;17809](https://github.com/element-hq/synapse/issues/17809))
* Check if user has membership in a room before tagging it. Contributed by Lama Alosaimi. ([#&#8203;17839](https://github.com/element-hq/synapse/issues/17839))
* Fix a bug in the admin redact endpoint where the background task would not run if a worker was specified in
* Fix bug where some presence and typing timeouts can expire early. ([#&#8203;17850](https://github.com/element-hq/synapse/issues/17850))
* Fix detection when the built Rust library was outdated when using source installations. ([#&#8203;17861](https://github.com/element-hq/synapse/issues/17861))
* Fix a long-standing bug in Synapse which could cause one-time keys to be issued in the incorrect order, causing message decryption failures. ([#&#8203;17903](https://github.com/element-hq/synapse/pull/17903))
* Fix experimental support for [MSC4222](https://github.com/matrix-org/matrix-spec-proposals/pull/4222) (Adding `state_after` to sync v2) where we would return the full state on incremental syncs when using lazy loaded members and there were no new events in the timeline. ([#&#8203;17915](https://github.com/element-hq/synapse/pull/17915))
* Remove support for python 3.8. ([#&#8203;17908](https://github.com/element-hq/synapse/issues/17908))
* Add a test for downloading and thumbnailing a CMYK JPEG. ([#&#8203;17786](https://github.com/element-hq/synapse/issues/17786))
* Refactor database calls to remove `Generator` usage. ([#&#8203;17813](https://github.com/element-hq/synapse/issues/17813), [#&#8203;17814](https://github.com/element-hq/synapse/issues/17814), [#&#8203;17815](https://github.com/element-hq/synapse/issues/17815), [#&#8203;17816](https://github.com/element-hq/synapse/issues/17816), [#&#8203;17817](https://github.com/element-hq/synapse/issues/17817), [#&#8203;17818](https://github.com/element-hq/synapse/issues/17818), [#&#8203;17890](https://github.com/element-hq/synapse/issues/17890))
* Include the destination in the error of 'Destination mismatch' on federation requests. ([#&#8203;17830](https://github.com/element-hq/synapse/issues/17830))
* The nix flake inside the repository no longer tracks nixpkgs/master to not catch the latest bugs from a MR merged 5 minutes ago. ([#&#8203;17852](https://github.com/element-hq/synapse/issues/17852))
* Minor speed-up of sliding sync by computing extensions results in parallel. ([#&#8203;17884](https://github.com/element-hq/synapse/issues/17884))
* Bump the default Python version in the Synapse Dockerfile from 3.11 -> 3.12. ([#&#8203;17887](https://github.com/element-hq/synapse/issues/17887))
* Remove usage of internal header encoding API. ([#&#8203;17894](https://github.com/element-hq/synapse/issues/17894))
* Use unique name for each os.arch variant when uploading Wheel artifacts. ([#&#8203;17905](https://github.com/element-hq/synapse/issues/17905))
* Fix tests to run with latest Twisted. ([#&#8203;17906](https://github.com/element-hq/synapse/pull/17906), [#&#8203;17907](https://github.com/element-hq/synapse/pull/17907), [#&#8203;17911](https://github.com/element-hq/synapse/pull/17911))
* Update version constraint to allow the latest poetry-core 1.9.1. ([#&#8203;17902](https://github.com/element-hq/synapse/pull/17902))
* Update the portdb CI to use Python 3.13 and Postgres 17 as latest dependencies. ([#&#8203;17909](https://github.com/element-hq/synapse/pull/17909))
* Add an index to `current_state_delta_stream` table. ([#&#8203;17912](https://github.com/element-hq/synapse/issues/17912))
* Fix building and attaching release artifacts during the release process. ([#&#8203;17921](https://github.com/element-hq/synapse/issues/17921))

[1.100.0]
* Update synapse to 1.120.0
* [Full Changelog](https://github.com/element-hq/synapse/releases/tag/v1.118.0)
* Fix a bug introduced in Synapse v1.120rc1 which would cause the newly-introduced `delete_old_otks` job to fail in worker-mode deployments. ([#&#8203;17960](https://github.com/element-hq/synapse/issues/17960))

[1.100.1]
* Update synapse to 1.120.2
* [Full Changelog](https://github.com/element-hq/synapse/releases/tag/v1.118.0)

[1.101.0]
* Update synapse to 1.121.1
* [Full Changelog](https://github.com/element-hq/synapse/releases/tag/v1.121.0)
* Support for MSC4190: device management for Application Services. (#17705)
* Update MSC4186 Sliding Sync to include invite, ban, kick, targets when $LAZY-loading room members. (#17947)
* Use stable M_USER_LOCKED error code for locked accounts, as per Matrix 1.12. (#17965)
* MSC4076: Add disable_badge_count to pusher configuration. (#17975)


[1.101.1]
* CLOUDRON_OIDC_PROVIDER_NAME implemented

[1.102.0]
* Update synapse to 1.122.0
* [Full Changelog](https://github.com/element-hq/synapse/releases/tag/v1.122.0)

[1.103.0]
* Update synapse to 1.123.0
* [Full Changelog](https://github.com/element-hq/synapse/releases/tag/v1.123.0)

[1.104.0]
* Update synapse to 1.124.0
* [Full Changelog](https://github.com/element-hq/synapse/releases/tag/v1.124.0)

[1.105.0]
* Update synapse to 1.125.0
* [Full Changelog](https://github.com/element-hq/synapse/releases/tag/v1.125.0)
* Add functionality to be able to use multiple values in SSO feature attribute_requirements. (#17949)
* Add experimental config options admin_token_path and client_secret_path for MSC3861. (#18004)
* Add get_current_time_msec() method to the module API for sound time comparisons with Synapse. (#18144)
* Update the response when a client attempts to add an invalid email address to the user's account from a 500, to a 400 with error text. (#18125)
* Fix user directory search when using a legacy module with a check_username_for_spam callback. Broke in v1.122.0. (#18135)

