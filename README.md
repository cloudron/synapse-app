## Federation notes

There are 2 ports - client port and federation port. On Cloudron, the client
port is reverse proxied, so we are good. The federation port is more complicated.

Matrix used to accept self-signed certs on the federation port before.
This was based on some 'perspectives' approach. This was deemed a failure
and they decided to go to having valid certs.

In [MSC1711](https://github.com/matrix-org/matrix-doc/blob/master/proposals/1711-x509-for-federation.md#background-the-failure-of-the-perspectives-approach), which happenned a Matrix 1.0 time, it was decided to use valid certs.
As part of this proposal, [MSC1708](https://github.com/matrix-org/matrix-doc/blob/master/proposals/1708-well-known-for-federation.md)
 aka .well-known support was added.

The idea of `.well-known` is delegation. Site X can delegate to Matrix Site Y.
Both require valid certs nevertheless.

The `_matrix._tcp.` SRV is:

```
$ host -t srv _matrix._tcp.example.org
_matrix._tcp.example.org has SRV record 0 0 8448 matrix.example.org.
```

The `.well-known/matrix/server` is:

```
{
   "m.server": "<server>[:<port>]"
}
```

Finally, to test federation go to [federationtester](https://federationtester.matrix.org/) - 
[source](https://github.com/matrix-org/matrix-federation-tester).

