#!/bin/bash

set -eu

mkdir -p /app/data/data /app/data/configs /run/synapse

if [[ ! -f /app/data/configs/homeserver.yaml ]]; then
    echo "==> Detected first run"

    # this is set at installation time and not changed after
    server_name=$(python -c "from publicsuffix2 import get_sld; print(get_sld('${CLOUDRON_APP_DOMAIN}'));")

    python3 -m synapse.app.homeserver \
        --server-name ${server_name} \
        --config-path /app/data/configs/homeserver.yaml \
        --config-directory /app/data/configs \
        --data-directory /app/data/data \
        --generate-config \
        --report-stats=no

    # fix logging configuration
    cp /app/pkg/homeserver.yaml.template /app/data/configs/homeserver.yaml
    mv /app/data/configs/${server_name}.log.config /app/data/configs/log.config
    yq eval -i ".log_config=\"/app/data/configs/log.config\"" /app/data/configs/homeserver.yaml

    # delete default file and buffer handlers
    yq eval -i "del(.handlers.file)" /app/data/configs/log.config
    yq eval -i "del(.handlers.buffer)" /app/data/configs/log.config

    mv /app/data/configs/${server_name}.signing.key /app/data/configs/signing.key

    yq eval -i ".server_name=\"${server_name}\"" /app/data/configs/homeserver.yaml
    yq eval -i ".registration_shared_secret=\"$(pwgen -1s 64)\"" /app/data/configs/homeserver.yaml

    yq eval -i ".auto_join_rooms=[]" /app/data/configs/homeserver.yaml
    yq eval -i ".auto_join_rooms[0]=\"#discuss:${server_name}\"" /app/data/configs/homeserver.yaml

    if [[ -z "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
        yq eval -i ".enable_registration=true" /app/data/configs/homeserver.yaml
        # just setting enabled to false is not enough. see https://github.com/matrix-org/matrix-synapse-ldap3/issues/123
        yq eval -i "del(.password_providers)" /app/data/configs/homeserver.yaml
    fi
    yq eval -i ".password_config.pepper=\"$(pwgen -1s 12)\"" /app/data/configs/homeserver.yaml # always set this so that users can enable password login if needed
fi

echo "==> Ensure we log to console"
yq eval -i ".root.handlers=[\"console\"]" /app/data/configs/log.config
yq eval -i ".loggers.twisted.handlers=[\"console\"]" /app/data/configs/log.config

[[ ! -f /app/data/index.html ]] && cp /app/pkg/index.html /app/data/index.html

echo "==> Configuring synapse"
yq eval -i ".public_baseurl=\"${CLOUDRON_APP_ORIGIN}\"" /app/data/configs/homeserver.yaml

# database
yq eval -i ".database.args.user=\"${CLOUDRON_POSTGRESQL_USERNAME}\"" /app/data/configs/homeserver.yaml
yq eval -i ".database.args.password=\"${CLOUDRON_POSTGRESQL_PASSWORD}\"" /app/data/configs/homeserver.yaml
yq eval -i ".database.args.database=\"${CLOUDRON_POSTGRESQL_DATABASE}\"" /app/data/configs/homeserver.yaml
yq eval -i ".database.args.host=\"${CLOUDRON_POSTGRESQL_HOST}\"" /app/data/configs/homeserver.yaml

# email
yq eval -i ".email.smtp_host=\"${CLOUDRON_MAIL_SMTP_SERVER}\"" /app/data/configs/homeserver.yaml
yq eval -i ".email.smtp_port=${CLOUDRON_MAIL_SMTP_PORT}" /app/data/configs/homeserver.yaml
yq eval -i ".email.smtp_user=\"${CLOUDRON_MAIL_SMTP_USERNAME}\"" /app/data/configs/homeserver.yaml
yq eval -i ".email.smtp_pass=\"${CLOUDRON_MAIL_SMTP_PASSWORD}\"" /app/data/configs/homeserver.yaml
yq eval -i ".email.notif_from=\"${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-Matrix} <${CLOUDRON_MAIL_FROM}>\"" /app/data/configs/homeserver.yaml

# oidc
if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    yq eval -i "del(.password_providers)" /app/data/configs/homeserver.yaml # remove old ldap config
    echo " ==> Configuring OIDC auth"
    yq eval -i ".oidc_providers[0].idp_id=\"cloudron\"" /app/data/configs/homeserver.yaml
    yq eval -i ".oidc_providers[0].idp_name=\"${CLOUDRON_OIDC_PROVIDER_NAME:-Cloudron}\"" /app/data/configs/homeserver.yaml
    yq eval -i ".oidc_providers[0].issuer=\"${CLOUDRON_OIDC_ISSUER}\"" /app/data/configs/homeserver.yaml
    yq eval -i ".oidc_providers[0].client_id=\"${CLOUDRON_OIDC_CLIENT_ID}\"" /app/data/configs/homeserver.yaml
    yq eval -i ".oidc_providers[0].client_secret=\"${CLOUDRON_OIDC_CLIENT_SECRET}\"" /app/data/configs/homeserver.yaml

    yq eval -i ".oidc_providers[0].scopes=[\"openid\", \"email\", \"profile\"]" /app/data/configs/homeserver.yaml
    yq eval -i ".oidc_providers[0].authorization_endpoint=\"${CLOUDRON_OIDC_AUTH_ENDPOINT}\"" /app/data/configs/homeserver.yaml
    yq eval -i ".oidc_providers[0].token_endpoint=\"${CLOUDRON_OIDC_TOKEN_ENDPOINT}\"" /app/data/configs/homeserver.yaml
    yq eval -i ".oidc_providers[0].userinfo_endpoint=\"${CLOUDRON_OIDC_PROFILE_ENDPOINT}\"" /app/data/configs/homeserver.yaml
    # https://s3lph.me/ldap-to-oidc-migration-3-matrix.html
    yq eval -i ".oidc_providers[0].allow_existing_users=true" /app/data/configs/homeserver.yaml
    yq eval -i ".oidc_providers[0].skip_verification=true" /app/data/configs/homeserver.yaml
    yq eval -i ".oidc_providers[0].user_mapping_provider.config.localpart_template=\"{{ user.sub }}\"" /app/data/configs/homeserver.yaml
    yq eval -i ".oidc_providers[0].user_mapping_provider.config.display_name_template=\"{{ user.name }}\"" /app/data/configs/homeserver.yaml
else
    yq eval -i ".password_config.localdb_enabled=true" /app/data/configs/homeserver.yaml
    # just setting enabled to false is not enough. see https://github.com/matrix-org/matrix-synapse-ldap3/issues/123
    yq eval -i "del(.password_providers)" /app/data/configs/homeserver.yaml
fi

# turn (https://github.com/matrix-org/synapse/blob/master/docs/turn-howto.md#synapse-setup)
if [[ -n "${CLOUDRON_TURN_SERVER:-}" ]]; then
    yq eval -i ".turn_uris=[]" /app/data/configs/homeserver.yaml
    yq eval -i ".turn_uris[0]=\"turn:${CLOUDRON_TURN_SERVER}:${CLOUDRON_TURN_TLS_PORT}?transport=udp\"" /app/data/configs/homeserver.yaml
    yq eval -i ".turn_uris[1]=\"turn:${CLOUDRON_TURN_SERVER}:${CLOUDRON_TURN_TLS_PORT}?transport=tcp\"" /app/data/configs/homeserver.yaml
    yq eval -i ".turn_shared_secret=\"${CLOUDRON_TURN_SECRET}\"" /app/data/configs/homeserver.yaml
fi

# fix permissions
echo "==> Fixing permissions"
chown -R cloudron.cloudron /app/data /run/synapse

echo "==> Starting synapse"
gosu cloudron:cloudron python3 -m synapse.app.homeserver --config-path /app/data/configs/homeserver.yaml -n
