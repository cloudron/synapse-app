Account ids are created with the username and the second level domain under which the
app is installed e.g. `@$CLOUDRON-USERNAME:$CLOUDRON-APP-DOMAIN`.

For federation to work, the delegation URI `https://$CLOUDRON-APP-DOMAIN/.well-known/matrix/server`
must be configured. See the [docs](https://docs.cloudron.io/apps/synapse/#post-installation) on how to do this.

