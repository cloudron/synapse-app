#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const ELEMENT_LOCATION = 'element-test';
    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;
    const ROOM_ID = Math.floor((Math.random() * 100) + 1);
    const ROOM_NAME = 'Test room ' + ROOM_ID;
    const MSG_TEXT = 'Test message ';

    let browser, app, elementApp;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');

        if (process.env.CI) execSync(`cloudron uninstall --app ${ELEMENT_LOCATION} || true`, EXEC_ARGS);
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    function getElementAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        elementApp = inspect.apps.filter(function (a) { return a.location.indexOf(ELEMENT_LOCATION) === 0; })[0];
        expect(elementApp).to.be.an('object');
    }

    function getMessage() {
        return MSG_TEXT + Math.floor((Math.random() * 100) + 1);
    }

    async function updateSynapseConfig() {
        console.log(`Setting Synapse Matrix server location to "https://${app.fqdn}"`);

        execSync(`cloudron exec --app ${ELEMENT_LOCATION} -- bash -c "jq '.default_server_config[\\"m.homeserver\\"].base_url = \\"https://${app.fqdn}\\"' /app/data/config.json | sponge /app/data/config.json"`);
        execSync(`cloudron restart --app ${ELEMENT_LOCATION}`);
        // wait when all services are up and running
        await browser.sleep(15000);
    }

    async function checkLandingPage() {
        await browser.get(`https://${app.fqdn}`);
        await browser.wait(until.elementLocated(By.xpath('//h1[contains(text(),"Synapse is running")]')), TEST_TIMEOUT);
    }

    async function registerUser() {
        await browser.get(`https://${elementApp.fqdn}/#/register`);
        await waitForElement(By.xpath('//input[@label="Username"]'));
        await browser.findElement(By.xpath('//input[@label="Username"]')).sendKeys(USERNAME);
        await browser.findElement(By.xpath('//input[@label="Password"]')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//input[@label="Confirm password"]')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//input[@value="Register"]')).click();

        await waitForElement(By.xpath('//h1[text()="You\'re in"] | //h1[contains(., "Welcome")]'));
        if (await browser.findElements(By.xpath('//div[@role="button" and text()="Skip"]')).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//div[@role="button" and text()="Skip"]')).click();
        }

        await waitForElement(By.xpath(`//h1[contains(., "Welcome")]`));
    }

    async function loginOIDC(username, password, alreadyAuthenticated, proceedWithReset) {
        browser.manage().deleteAllCookies();
        await browser.get(`https://${elementApp.fqdn}/#/login`);
        await browser.sleep(2000);

        await waitForElement(By.css('.mx_Dropdown_arrow'));
        await browser.findElement(By.css('.mx_Dropdown_arrow')).click();
        await waitForElement(By.id('mx_LanguageDropdown__en'));
        await browser.findElement(By.id('mx_LanguageDropdown__en')).click();
        await browser.sleep(3000);

        await waitForElement(By.xpath('//div[@role="button" and contains(., "Continue with")]'));
        await browser.findElement(By.xpath('//div[@role="button" and contains(., "Continue with")]')).click();
        if (!alreadyAuthenticated) {
            await waitForElement(By.xpath('//input[@name="username"]'));
            await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
            await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }

        await waitForElement(By.xpath('//p[@class="confirm-trust" and contains(., "Continuing will grant ")]'));
        await browser.findElement(By.xpath('//a[contains(., "Continue")]')).click();

        if (proceedWithReset) {
            await waitForElement(By.xpath('//div[text()="Proceed with reset" or text()="Reset all"]'));

            if (await browser.findElements(By.xpath('//div[text()="Reset all"]')).then(found => !!found.length)) {
                await browser.findElement(By.xpath('//div[text()="Reset all"]')).click();
            }

            await waitForElement(By.xpath('//div[text()="Proceed with reset"]'));
            await browser.findElement(By.xpath('//div[text()="Proceed with reset"]')).click();

            await waitForElement(By.xpath('//button[@class="mx_Dialog_primary" and text()="Continue"]'));
            await browser.findElement(By.xpath('//button[@class="mx_Dialog_primary" and text()="Continue"]')).click();

            await waitForElement(By.xpath('//div[text()="Copy"]'));
            await browser.findElement(By.xpath('//div[text()="Copy"]')).click();

            await waitForElement(By.xpath('//button[@class="mx_Dialog_primary" and text()="Continue"]'));
            await browser.findElement(By.xpath('//button[@class="mx_Dialog_primary" and text()="Continue"]')).click();
            await waitForElement(By.xpath('//button[text()="Done"] | //div[text()="Single Sign On"]'));

            if (await browser.findElements(By.xpath('//div[text()="Single Sign On"]')).then(found => !!found.length)) {

                await browser.findElement(By.xpath('//div[text()="Single Sign On"]')).click();

                const originalWindowHandle = await browser.getWindowHandle();
                await browser.wait(async () => (await browser.getAllWindowHandles()).length === 2, 10000);
                //Loop through until we find a new window handle
                const windows = await browser.getAllWindowHandles();
                windows.forEach(async handle => {
                    if (handle !== originalWindowHandle) {
                        await browser.switchTo().window(handle);
                    }
                });
                await waitForElement(By.xpath('//a[contains(., "Continue with")]'));
                await browser.findElement(By.xpath('//a[contains(., "Continue with")]')).click();

                // switch back to the main window
                await browser.switchTo().window(originalWindowHandle);

                await waitForElement(By.xpath('//div[text()="Confirm"]'));
                await browser.findElement(By.xpath('//div[text()="Confirm"]')).click();
            }

            await waitForElement(By.xpath('//button[text()="Done"]'));
            await browser.findElement(By.xpath('//button[text()="Done"]')).click();

            await waitForElement(By.xpath('//div[text()="Cancel"] | //h1[contains(., "Welcome")]'));
            if (await browser.findElements(By.xpath('//div[text()="Cancel"]')).then(found => !!found.length)) {
                await browser.findElement(By.xpath('//div[text()="Cancel"]')).click();
            }
        }

        await browser.sleep(3000);
        await waitForElement(By.xpath(`//h1[contains(., "Welcome")]`));
    }

    async function login() {
        await browser.get(`https://${elementApp.fqdn}/#/login`);
        await browser.wait(until.elementLocated(By.xpath('//input[@value="Sign in"]')), TEST_TIMEOUT);
        await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(USERNAME);
        await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//input[@value="Sign in"]')).click();
        await browser.sleep(5000);
        await skipVerification();
        await browser.wait(until.elementLocated(By.xpath('//span[text()="Rooms"]')), TEST_TIMEOUT);
    }

    async function skipVerification() {
        await browser.wait(until.elementLocated(By.xpath('//div[@aria-label="Skip verification for now"]')), TEST_TIMEOUT);
        await browser.sleep(5000);
        await browser.findElement(By.xpath('//div[@aria-label="Skip verification for now"]')).click();
        await browser.wait(until.elementLocated(By.xpath('//div[contains(text(), "verify later")]')), TEST_TIMEOUT);
        await browser.sleep(5000);
        await browser.findElement(By.xpath('//div[contains(text(), "verify later")]')).click();
        await browser.sleep(5000);
    }

    async function logout() {
        await browser.get(`https://${elementApp.fqdn}/#/home`);
        await browser.sleep(5000);
        await waitForElement(By.xpath('//div[@role="button" and @aria-label="User menu"]'));

        await browser.findElement(By.xpath('//div[@role="button" and @aria-label="User menu"]')).click();
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//li[@role="menuitem" and @aria-label="Sign out"]')).click();
        await browser.sleep(2000);

        if (await browser.findElements(By.xpath('//button[contains(text(), "I don\'t want my encrypted messages")]')).then(found => !!found.length)) {
            await browser.findElement(By.xpath('//button[contains(text(), "I don\'t want my encrypted messages")]')).click();
            await browser.sleep(3000);
        }

        await waitForElement(By.xpath('//h1[text()="Sign in"]'));
    }

    async function isLoggedIn() {
        await browser.get(`https://${elementApp.fqdn}/#/home`);
        await browser.wait(until.elementLocated(By.xpath('//span[text()="Rooms"]')), TEST_TIMEOUT);
    }

    async function createRoom() {
        await browser.get(`https://${elementApp.fqdn}/#/home`);
        await browser.sleep(2000);
        await waitForElement(By.xpath('//div[@role="button" and @aria-label="Add room"]'));
        await browser.findElement(By.xpath('//div[@role="button" and @aria-label="Add room"]')).click();
        await browser.sleep(1000);
        await waitForElement(By.xpath('//li[@role="menuitem" and @aria-label="New room"]'));
        await browser.findElement(By.xpath('//li[@role="menuitem" and @aria-label="New room"]')).click();
        await browser.sleep(1000);

        await waitForElement(By.xpath('//input[@label="Name"]'));
        await browser.findElement(By.xpath('//input[@label="Name"]')).sendKeys(ROOM_NAME);

        await browser.sleep(1000);

        await waitForElement(By.xpath('//button[text()="Create room"]'));
        await browser.findElement(By.xpath('//button[text()="Create room"]')).click();

        await browser.sleep(1000);

        await waitForElement(By.xpath('//div[@role="button" and @aria-label="Add room"]'));

        await waitForElement(By.xpath('//div[@class="mx_RoomTile_titleContainer"]/div[@title="' + ROOM_NAME + '"]'));
    }

    async function checkRoom() {
        await browser.get(`https://${elementApp.fqdn}/#/home`);
        await browser.sleep(4000);
        await waitForElement(By.xpath('//div[@role="treeitem" and @aria-label="' + ROOM_NAME + '"]'));
        await browser.findElement(By.xpath('//div[@role="treeitem" and @aria-label="' + ROOM_NAME + '"]')).click();
        await browser.sleep(2000);
        await waitForElement(By.xpath('//h2[text()="' + ROOM_NAME + '"]'));
    }

    async function sendMessage() {
        await checkRoom();

        await browser.findElement(By.xpath('//div[contains(@class, "mx_BasicMessageComposer_input")]')).sendKeys(getMessage());
        await browser.sleep(2000);

        await browser.findElement(By.xpath('//div[@role="button" and @aria-label="Send message"]')).click();
        await browser.sleep(2000);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });

    // No SSO
    it('install app (no sso)', function () { execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS); });
    it('can get app information', getAppInfo);
    it('check landing page', checkLandingPage);

    it('can install element-web app (no sso)', function () { execSync('cloudron install --appstore-id im.riot.cloudronapp --location ' + ELEMENT_LOCATION, EXEC_ARGS); });
    it('update element-app config', updateSynapseConfig);

    it('can get Element app info', getElementAppInfo);
    it('can register new user', registerUser);

    it('create room', createRoom);
    it('can send message', sendMessage);
    it('can logout', logout); // from auto-login

    it('can login', login);
    it('check room', checkRoom);
    it('can logout', logout);

    it('uninstall element-web app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${ELEMENT_LOCATION}`, EXEC_ARGS);
    });
    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // SSO
    it('install app (sso)', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });
    it('can get app info', getAppInfo);

    it('can install element-web app (sso)', function () { execSync('cloudron install --appstore-id im.riot.cloudronapp --location ' + ELEMENT_LOCATION, EXEC_ARGS); });
    it('can get Element app info', getElementAppInfo);
    it('update element-app config', updateSynapseConfig);

    it('can login via OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false, false));
    it('create room', createRoom);
    it('can send message', sendMessage);
    it('can get app info', getAppInfo);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });

    it('is logged in', isLoggedIn);
    it('check room', checkRoom);

    it('restore app', async function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));

        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);

        getAppInfo();

        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('is logged in', isLoggedIn);
    it('check room', checkRoom);
    it('can send message', sendMessage);
    it('can logout', logout);
    it('can get app info', getAppInfo);

    // web ui also throws random errors after changing domain
    xit('move to different location (skipped since no matrix support)', async function () {
        browser.manage().deleteAllCookies();
        await browser.get('about:blank');

        execSync(`cloudron configure --location ${LOCATION}2`, EXEC_ARGS);
        getAppInfo();
        await browser.sleep(15000);
    });
    xit('update element-app config', updateSynapseConfig);
    xit('can get Element app info', getElementAppInfo);
    xit('can login via OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, true, true));
    xit('check room', checkRoom);
    xit('can send message', sendMessage);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    it('uninstall element-web app', function () {
        execSync(`cloudron uninstall --app ${ELEMENT_LOCATION}`, EXEC_ARGS);
    });

    // test update
    it('clear cache', clearCache);
    it('can install app for update', function () { execSync('cloudron install --appstore-id org.matrix.synapse --location ' + LOCATION, EXEC_ARGS); });
    it('can get app info', getAppInfo);

    it('can install element-web app (update)', function () { execSync('cloudron install --appstore-id im.riot.cloudronapp --location ' + ELEMENT_LOCATION, EXEC_ARGS); });
    it('can get Element app info', getElementAppInfo);
    it('update element-app config', updateSynapseConfig);

    it('can login via OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false, false));
    it('is logged in', isLoggedIn);
    it('create room', createRoom);
    it('can send message', sendMessage);
    it('can logout', logout);
    it('clear cache', clearCache);

    it('can update', async function () {
        await browser.get('about:blank');
        execSync(`cloudron update --app ${app.id}`, EXEC_ARGS);
        await browser.sleep(15000);
    });

    it('can get Element app info', getElementAppInfo);
    it('can login via OIDC', loginOIDC.bind(null, USERNAME, PASSWORD, false, true));

    it('is logged in', isLoggedIn);
    it('check room', checkRoom);

    it('can send message', sendMessage);
    it('can get app info', getAppInfo);

    it('uninstall app', async function () {
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    it('uninstall element-web app', function () {
        execSync(`cloudron uninstall --app ${ELEMENT_LOCATION}`, EXEC_ARGS);
    });
});
