FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/pkg

WORKDIR /app/code

# https://pythonspeed.com/articles/activate-virtualenv-dockerfile/
RUN virtualenv -p python3 /app/code/env
ENV VIRTUAL_ENV=/app/code/env
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

# renovate: datasource=github-releases depName=element-hq/synapse versioning=semver extractVersion=^v(?<version>.+)$
ARG SYNAPSE_VERSION=1.125.0

# renovate: datasource=github-releases depName=matrix-org/synapse-s3-storage-provider versioning=semver extractVersion=^v(?<version>.+)$
ARG S3PROVIDER_VERSION=1.5.0

# Synapse (https://github.com/matrix-org/synapse/blob/master/INSTALL.md)
# lxml - required for previews
RUN pip install --upgrade pip && \
    pip install --upgrade setuptools && \
    pip install matrix-synapse==v${SYNAPSE_VERSION} psycopg2-binary python-ldap matrix-synapse-ldap3 lxml publicsuffix2 git+https://github.com/matrix-org/synapse-s3-storage-provider.git@v${S3PROVIDER_VERSION} matrix-synapse[oidc]

# Updated suffix list
RUN curl -L https://publicsuffix.org/list/public_suffix_list.dat -o /app/code/env/lib/python3.10/site-packages/publicsuffix2/public_suffix_list.dat

RUN ln -sf /app/data/index.html /app/code/env/lib/python3.10/site-packages/synapse/static/index.html

RUN chown -R cloudron.cloudron /app/code

ADD index.html homeserver.yaml.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
